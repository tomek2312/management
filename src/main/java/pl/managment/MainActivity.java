package pl.managment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.common.base.Optional;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Person;
import pl.managment.db.enums.PersonFunction;
import pl.managment.fragments.PeopleFragment;
import pl.managment.fragments.ProfileFragment;
import pl.managment.fragments.ProjectsListFragment;
import pl.managment.fragments.TasksFragment;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_main)
public class MainActivity extends RoboEventToolbarActivity {
    private static final int PROFILE = 1;
    private static final int PROJECTS = 2;
    private static final int TASKS = 3;
    private static final int PEOPLE = 4;
    private int mCurrentFragment;

    @InjectView(R.id.drawer_layout)
    private DrawerLayout mDrawerLayout;

    @InjectView(R.id.imgvBlurredAvatar)
    private ImageView mImgvBlurredAvatar;

    @InjectView(R.id.imgvAvatar)
    private ImageView mImgvAvatar;

    @InjectView(R.id.txtvUserName)
    private TextView mTxtvUserName;

    @InjectView(R.id.txtvUserFunction)
    private TextView mTxtvUserPosition;

    @InjectView(R.id.llDrawerProfile)
    private LinearLayout mLlDrawerProfile;

    @InjectView(R.id.llDrawerProjects)
    private LinearLayout mLlDrawerProjects;

    @InjectView(R.id.llDrawerTasks)
    private LinearLayout mLlDrawerTasks;

    @InjectView(R.id.llDrawerSettings)
    private LinearLayout mLlDrawerSettings;

    @InjectView(R.id.llDrawerPeople)
    private LinearLayout mLlDrawerPeople;

    private ActionBarDrawerToggle mDrawerToggle;

    @Inject
    private ImageUtils mImageUtils;

    private boolean mIsDoubleClick;
    private boolean mIsMenu;
    private boolean mIsDrawer;
    private int mPersonId;

    private void closeDrawer() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDrawerLayout.closeDrawers();
            }
        }, 400);
    }

    /**
     * Method hides keyboard if is visible.
     */
//    public void hideKeyboard() {
//        InputMethodManager inputManager = (InputMethodManager) getSystemService(
//                Context.INPUT_METHOD_SERVICE);
//
//        View view = getCurrentFocus();
//        if (view != null) {
//            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
//                    InputMethodManager.HIDE_NOT_ALWAYS);
//        }
//    }
    private void setPopup(Point p) {
        LayoutInflater layoutInflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_menu, null);
        LinearLayout llProfile = (LinearLayout) layout.findViewById(R.id.llPopupProfile);
        LinearLayout llProjects = (LinearLayout) layout.findViewById(R.id.llPopupProjects);
        LinearLayout llTasks = (LinearLayout) layout.findViewById(R.id.llPopupTasks);
        LinearLayout llSettings = (LinearLayout) layout.findViewById(R.id.llPopupSettings);

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(MainActivity.this);
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the left, and a bit down, relative to button's position.
        int offset = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8,
                MyApplication.getAppContext().getResources().getDisplayMetrics());

        //Clear the default translucent background
        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x - offset, p.y + offset);

        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMainFragment(ProfileFragment.getInstance(mPersonId), false);
                popup.dismiss();
            }
        });

        llProjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMainFragment(new ProjectsListFragment(), false);
                popup.dismiss();
            }
        });

        llTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMainFragment(TasksFragment.getInstance(mPersonId), false);
                popup.dismiss();
            }
        });

        llSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                logOut();
            }
        });
    }


    @Override
    public void setFragment(Fragment fragment, boolean addToStack) {
        if (addToStack) {
            mStack++;
        }

        super.setFragment(fragment, addToStack);
    }

    public void setMainFragment(Fragment fragment, boolean addToStack) {
        if (fragment instanceof ProfileFragment && mCurrentFragment != PROFILE) {
            mCurrentFragment = PROFILE;
            setFragment(fragment, addToStack);
        }

        if (fragment instanceof ProjectsListFragment && mCurrentFragment != PROJECTS) {
            mCurrentFragment = PROJECTS;
            setFragment(fragment, addToStack);
        }

        if (fragment instanceof TasksFragment && mCurrentFragment != TASKS) {
            mCurrentFragment = TASKS;
            setFragment(fragment, addToStack);
        }

        if (fragment instanceof PeopleFragment && mCurrentFragment != PEOPLE) {
            mCurrentFragment = PEOPLE;
            setFragment(fragment, addToStack);
        }
    }

    private void logOut() {
        MyApplication.clearSetting(BundleConstants.LOGIN_PERSON_ID);
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void showPopup() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int y = size.y - mFlContainer.getMeasuredHeight();
        int x = mFlContainer.getRight() - (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 200,
                MyApplication.getAppContext().getResources().getDisplayMetrics());

        Point point = new Point();
        point.x = x;
        point.y = y;
        setPopup(point);
    }

    private void setDrawer(final String path) {
        AsyncTask<Void, Void, Person> loader = new AsyncTask<Void, Void, Person>() {
            private Bitmap mAvatar;
            private Bitmap mBlurAvatar;

            @Override
            protected void onPreExecute() {
                mAvatar = BitmapFactory.decodeResource(getResources(), R.drawable.male_avatar);
                mBlurAvatar = BitmapFactory.decodeResource(getResources(), R.drawable.male_avatar);
            }

            @Override
            protected Person doInBackground(Void... params) {
                mBlurAvatar = mImageUtils.blurImage(
                        MyApplication.getAppContext(), mBlurAvatar, 25);

                PersonDataSource dataSource = PersonDataSource.getInstance();
                List<Person> people = dataSource.getAll();
                for (Person person : people) {
                    if (mPersonId == person.getId()) {
                        if (person.getAvatar() != null && !TextUtils.isEmpty(person.getAvatar())) {
                            String newPath = !TextUtils.isEmpty(path) ? path : person.getAvatar();
                            File file = new File(newPath);
                            Bitmap newBitmap = mImageUtils.getResizedBitmap(mAvatar.getWidth(), mAvatar.getHeight(), file.getAbsolutePath());
                            if (newBitmap != null) {
                                mAvatar = mImageUtils.transformToOval(MyApplication.getAppContext(),
                                        mImageUtils.cropBitmapToSquare(newBitmap), R.drawable.ic_profile_mask);
                                mBlurAvatar = mImageUtils.blurImage(
                                        MyApplication.getAppContext(), newBitmap, 25);
                            }

                        }
                        return person;
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Person person) {
                mImgvBlurredAvatar.setImageBitmap(mBlurAvatar);
                mImgvAvatar.setImageBitmap(mAvatar);
                if (person != null) {
                    mTxtvUserName.setText(person.getName() + " " +
                            person.getSurname());
                    mTxtvUserPosition.setText(person.getFunction().getName());

                    mLlDrawerPeople.setVisibility(person.getFunction() == PersonFunction.ADMIN ?
                            View.VISIBLE : View.GONE);
                }
            }
        };

        loader.execute();
    }

    private void checkProfile() {
        Optional<String> profile = MyApplication.loadFromSettings(BundleConstants.LOGIN_PERSON_ID);
        if (profile.isPresent()) {
            mPersonId = Integer.parseInt(profile.get());
            loadFromIntent();
        } else {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void loadFromIntent() {
//        Intent intent = getIntent();
//        if (intent != null) {
//            Bundle extras = intent.getExtras();
//            if (extras != null) {
//                if (extras.containsKey(BundleConstants.EXTRA_DRAWER)) {
        mIsDrawer = true;
        setDrawerToggle();
        setMainFragment(ProfileFragment.getInstance(mPersonId), false);
        setDrawer("");
        setDrawerListeners();
//                }
//
//                if (extras.containsKey(BundleConstants.EXTRA_MENU)) {
//                    mIsMenu = true;
//                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//                    invalidateOptionsMenu();
//                    setMainFragment(ProfileFragment.getInstance(mPersonId), false);
//                }
//
//                if (extras.containsKey(BundleConstants.EXTRA_TABS)) {
//                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//                    setFragment(TabFragment.getInstance(mPersonId), false);
//                }
//            }
//        }
    }

    private void setDrawerListeners() {
        mLlDrawerProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMainFragment(ProfileFragment.getInstance(mPersonId), false);
                closeDrawer();
            }
        });

        mLlDrawerProjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMainFragment(new ProjectsListFragment(), false);
                closeDrawer();
            }
        });

        mLlDrawerTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMainFragment(TasksFragment.getInstance(mPersonId), false);
//                setFragment(CampaignTaskPagerFragment.getInstance(mPersonId), false);
                closeDrawer();
            }
        });

        mLlDrawerSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
                closeDrawer();
            }
        });

        mLlDrawerPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMainFragment(PeopleFragment.getInstance(mPersonId), false);
                closeDrawer();
            }
        });
    }

    /**
     * Method sets drawer toggle.
     */
    private void setDrawerToggle() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkProfile();

        JsonRequest objectRequest = new JsonRequest(Request.Method.GET,
                BundleConstants.API_USERS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                showToast("sukces");
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
                        if (error instanceof NoConnectionError) {
                            showToast("nie ma internetu");
                        } else {
                            showToast("blad serwera");
                        }
                    }
                }) {
        };

//        MyApplication.getInstance().getRequestQueue().add(objectRequest);
    }

    @Override
    public void onBackPressed() {
        if (mIsDoubleClick || getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        } else {
            showToast("Kliknij dwukrotnie aby wyjść");
            mIsDoubleClick = true;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mIsDoubleClick = false;
                }
            }, 1500);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mIsDrawer) {
            mDrawerToggle.setDrawerIndicatorEnabled(
                    mStack == 0);
        }

        if (mIsMenu) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            showPopup();

            return true;
        }

        if (mIsDrawer) {
            return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mIsDrawer) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mIsDrawer) {
            mDrawerToggle.onConfigurationChanged(newConfig);
        }
    }

    @Subscribe
    public void changeProfileAvatar(final AvatarEvent event) {
        setDrawer(event.getPath());
    }
}
