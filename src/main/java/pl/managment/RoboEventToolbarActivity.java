package pl.managment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import roboguice.inject.InjectView;

/**
 * Activity that extends CompatActivity.
 * Sets automatically Toolbar.
 *
 * @author Tomasz Trybała
 */
public class RoboEventToolbarActivity extends RoboEventCompatActivity {
    private static final String DEFAULT_MESSAGE = "Loading...";
    protected static int mStack;

    @InjectView(R.id.flContainer)
    protected FrameLayout mFlContainer;

    private ProgressDialog mDialog;

    public void setActionBarTitle(String title) {
        setTitle(title);
    }

    @SuppressLint("InflateParams")
    private void prepareLayout() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Method sets progress dialog, which is shown
     * when data from server is loading.
     */
    private void setProgressDialog() {
        mDialog = new ProgressDialog(this);
        mDialog.setCancelable(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepareLayout();
        setProgressDialog();
    }


    /**
     * Method hides keyboard if is visible.
     */
    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);

        View view = getCurrentFocus();
        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void setFragment(Fragment fragment, boolean addToStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(mFlContainer.getId(), fragment);
        if (addToStack) {
            ft.addToBackStack(fragment.getClass().getName());
        }
        ft.commit();
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == android.R.id.home) {
            popBackStack();
            return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        popBackStack();
    }

    protected void popBackStack() {
        hideKeyboard();
        if (mStack > 0) {
            mStack--;
        }

        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
            invalidateOptionsMenu();
        } else {
            finish();
        }
    }

    public void clearBackStackToFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(fragment.getClass().getName(), 0);
    }

    /**
     * Method shows progress dialog.
     */
    public void showDialog() {
        if (!mDialog.isShowing()) {
            mDialog.setMessage(DEFAULT_MESSAGE);
            mDialog.show();
        } else {
            Runnable changeMessage = new Runnable() {
                @Override
                public void run() {
                    mDialog.setMessage(DEFAULT_MESSAGE);
                }
            };
            runOnUiThread(changeMessage);
        }
    }

    /**
     * Method shows progress dialog with custom message.
     */
    public void showDialog(final String message) {
        if (!mDialog.isShowing()) {
            mDialog.setMessage(message);
            mDialog.show();
        } else {
            Runnable changeMessage = new Runnable() {
                @Override
                public void run() {
                    mDialog.setMessage(message);
                }
            };
            runOnUiThread(changeMessage);
        }
    }

    /**
     * Method hides progress dialog.
     */
    public void hideDialog() {
        if (mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    /**
     * Method shows toast.
     *
     * @param message message to user.
     */
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}