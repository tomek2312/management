
package pl.managment;

/**
 * @author Tomasz Trybała
 * @since 03.03.2015
 */
public class BundleConstants {
    public static final int AVATAR_SIZE = 400;
    public static final String LOGIN_PERSON_ID = "login_person_id";
    public final static String FILE_USER_PROFILE_AVATAR = "user_profile_avatar";

    public final static String EXTRA_TABS = "extra_tabs";
    public final static String EXTRA_DRAWER = "extra_drawer";
    public final static String EXTRA_MENU = "extra_menu";

    public final static String API_USERS = "http://tt-server.herokuapp.com/users/";


}
