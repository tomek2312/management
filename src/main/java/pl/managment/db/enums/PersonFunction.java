package pl.managment.db.enums;

/**
 * @author Tomasz Trybała
 */
public enum PersonFunction {
    ADMIN("admin"),
    MANAGER("manager"),
    WORKER("worker");

    private String mName;

    private PersonFunction(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }
}
