package pl.managment.db.enums;

/**
 * @author Tomasz Trybała
 */
public enum ExercisePriority {
    LOW("low"),
    NORMAL("normal"),
    HIGH("high");

    private String mName;

    private ExercisePriority(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public String getDisplayName() {
        switch (mName) {
            case "low":
                return "Niski";
            case "normal":
                return "Normalny";
            default:
                return "Wysoki";
        }
    }
}
