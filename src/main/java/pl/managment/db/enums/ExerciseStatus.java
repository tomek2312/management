package pl.managment.db.enums;

/**
 * @author Tomasz Trybała
 */
public enum ExerciseStatus {
    CREATE("create"),
    IN_PROGRESS("in_progress"),
    CLOSED("closed");

    private String mName;

    private ExerciseStatus(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public String getDisplayName() {
        switch (mName) {
            case "create":
                return "Utworzone";
            case "in_progress":
                return "W trakcie";
            default:
                return "Zamknięte";
        }
    }
}
