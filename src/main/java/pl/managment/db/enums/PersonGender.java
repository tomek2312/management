package pl.managment.db.enums;

/**
 * @author Tomjasz Trybała
 */
public enum PersonGender {
    MALE("male"),
    FEMALE("female");

    private String mName;

    private PersonGender(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }
}
