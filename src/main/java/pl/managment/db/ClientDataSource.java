package pl.managment.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

import pl.managment.db.enitity.Client;
import pl.managment.db.table.ClientTable;

/**
 * @author Tomasz Trybała
 */
public class ClientDataSource {
    private static ClientDataSource sDataSource;
    private SQLiteDatabase mDatabase;

    private ClientDataSource() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance();
        mDatabase = dbHelper.getDatabase();
    }

    private Client cursorToClient(Cursor c) {
        int id;
        String name, phone, person;

        id = c.getInt(ClientTable.ColumnID.CLIENT_ID);
        name = c.getString(ClientTable.ColumnID.NAME);
        phone = c.getString(ClientTable.ColumnID.PHONE);
        person = c.getString(ClientTable.ColumnID.PERSON);

        return new Client(id, name, phone, person);
    }

    public static ClientDataSource getInstance() {
        if (sDataSource == null) {
            sDataSource = new ClientDataSource();
        }

        return sDataSource;
    }

    public Client get(int clientId) {
        String where = String.format("%s = %d", ClientTable.Column.CLIENT_ID, clientId);
        Cursor c = mDatabase.query(ClientTable.TABLE_NAME, ClientTable.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        if (c.getCount() > 0) {
            Client client = cursorToClient(c);
            c.close();

            return client;
        } else {
            c.close();
            return null;
        }
    }

    public List<Client> getAll() {
        Cursor c = mDatabase.query(ClientTable.TABLE_NAME, ClientTable.ALL_COLUMNS, null, null, null, null, null);
        c.moveToFirst();

        List<Client> clients = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                clients.add(cursorToClient(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return clients;
    }

    public void insert(Client client, boolean isId) {
        ContentValues values = new ContentValues();
        if (isId) {
            values.put(ClientTable.Column.CLIENT_ID, client.getId());
        }
        values.put(ClientTable.Column.NAME, client.getName());
        values.put(ClientTable.Column.PERSON, client.getPerson());
        values.put(ClientTable.Column.PHONE, client.getPhone());

        mDatabase.insertWithOnConflict(ClientTable.TABLE_NAME, null, values,
                SQLiteDatabase.CONFLICT_REPLACE);
    }
}
