package pl.managment.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

import pl.managment.db.enitity.Session;
import pl.managment.db.table.SessionsTable;

/**
 * @author Tomasz Trybała
 */
public class SessionsDataSource {
    private static SessionsDataSource sDataSource;
    private SQLiteDatabase mDatabase;

    private SessionsDataSource() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance();
        mDatabase = dbHelper.getDatabase();
    }

    private Session cursorToSession(Cursor c) {
        int id, exerciseId;
        String start, stop, description;

        id = c.getInt(SessionsTable.ColumnID.SESSION_ID);
        exerciseId = c.getInt(SessionsTable.ColumnID.EXERCISE_ID);
        start = c.getString(SessionsTable.ColumnID.START);
        stop = c.getString(SessionsTable.ColumnID.STOP);
        description = c.getString(SessionsTable.ColumnID.DESCRIPTION);

        return new Session(id, exerciseId, start, stop, description);
    }

    public static SessionsDataSource getInstance() {
        if (sDataSource == null) {
            sDataSource = new SessionsDataSource();
        }

        return sDataSource;
    }

    public Session get(int sessionId) {
        String where = String.format("%s = %d", SessionsTable.Column.SESSION_ID, sessionId);
        Cursor c = mDatabase.query(SessionsTable.TABLE_NAME, SessionsTable.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        if (c.getCount() > 0) {
            Session session = cursorToSession(c);
            c.close();

            return session;
        } else {
            c.close();
            return null;
        }
    }

    public List<Session> getAll() {
        Cursor c = mDatabase.query(SessionsTable.TABLE_NAME, SessionsTable.ALL_COLUMNS, null, null, null, null, null);
        c.moveToFirst();

        List<Session> sessions = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                sessions.add(cursorToSession(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return sessions;
    }

    public List<Session> getAllByeExercise(int exerciseId) {
        String where = String.format("%s = %d", SessionsTable.Column.EXERCISE_ID, exerciseId);
        String orderBy = String.format("%s DESC", SessionsTable.Column.START);
        Cursor c = mDatabase.query(SessionsTable.TABLE_NAME, SessionsTable.ALL_COLUMNS, where, null, null, null, orderBy);
        c.moveToFirst();

        List<Session> sessions = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                sessions.add(cursorToSession(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return sessions;
    }

    public void insert(Session session) {
        ContentValues values = new ContentValues();
        values.put(SessionsTable.Column.DESCRIPTION, session.getDescription());
        values.put(SessionsTable.Column.EXERCISE_ID, session.getExerciseId());
        values.put(SessionsTable.Column.START, session.getStart());
        values.put(SessionsTable.Column.STOP, session.getStop());

        mDatabase.insertWithOnConflict(SessionsTable.TABLE_NAME, null, values,
                SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void delete(int id){
        String where = String.format("%s = %d", SessionsTable.Column.SESSION_ID, id);
        mDatabase.delete(SessionsTable.TABLE_NAME, where, null);
    }
}
