package pl.managment.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

import pl.managment.db.enitity.Exercise;
import pl.managment.db.table.ExerciseTable;
import pl.managment.db.table.SessionsTable;

/**
 * @author Tomasz Trybała
 */
public class ExerciseDataSource {
    private static ExerciseDataSource sDataSource;
    private SQLiteDatabase mDatabase;

    private ExerciseDataSource() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance();
        mDatabase = dbHelper.getDatabase();
    }

    private Exercise cursorToExercise(Cursor c) {
        int id, personId, progress, campaignId;
        String created, name, description, status, approximateTime, priority;

        id = c.getInt(ExerciseTable.ColumnID.EXERCISE_ID);
        personId = c.getInt(ExerciseTable.ColumnID.PERSON_ID);
        progress = c.getInt(ExerciseTable.ColumnID.PROGRESS);
        campaignId = c.getInt(ExerciseTable.ColumnID.CAMPAIGN_ID);
        name = c.getString(ExerciseTable.ColumnID.NAME);
        description = c.getString(ExerciseTable.ColumnID.DESCRIPTION);
        status = c.getString(ExerciseTable.ColumnID.STATUS);
        approximateTime = c.getString(ExerciseTable.ColumnID.TIME);
        priority = c.getString(ExerciseTable.ColumnID.PRIORITY);
        created = c.getString(ExerciseTable.ColumnID.CREATED);


        return new Exercise(id, name, description, personId, status, approximateTime,
                progress, priority, campaignId, created);
    }

    public void insert(Exercise exercise, boolean isId) {
        ContentValues values = new ContentValues();
        if (exercise.getPersonId() > -1) {
            values.put(ExerciseTable.Column.PERSON_ID, exercise.getPersonId());
        }
        values.put(ExerciseTable.Column.PROGRESS, exercise.getProgress());
        values.put(ExerciseTable.Column.CAMPAIGN_ID, exercise.getCampaignId());
        values.put(ExerciseTable.Column.NAME, exercise.getName());
        values.put(ExerciseTable.Column.DESCRIPTION, exercise.getDescription());
        values.put(ExerciseTable.Column.STATUS, exercise.getStatus().getName());
        values.put(ExerciseTable.Column.TIME, exercise.getAproximateTime());
        values.put(ExerciseTable.Column.PRIORITY, exercise.getPriority().getName());
        values.put(ExerciseTable.Column.CREATED, exercise.getCreated());
        if (isId) {
            values.put(ExerciseTable.Column.EXERCISE_ID, exercise.getId());
        }

        mDatabase.insertWithOnConflict(ExerciseTable.TABLE_NAME, null, values,
                SQLiteDatabase.CONFLICT_REPLACE);
    }

    public static ExerciseDataSource getInstance() {
        if (sDataSource == null) {
            sDataSource = new ExerciseDataSource();
        }

        return sDataSource;
    }

    public Exercise get(int exerciseId) {
        String where = String.format("%s = %d", ExerciseTable.Column.EXERCISE_ID, exerciseId);
        Cursor c = mDatabase.query(ExerciseTable.TABLE_NAME, ExerciseTable.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        if (c.getCount() > 0) {
            Exercise exercise = cursorToExercise(c);
            c.close();

            return exercise;
        } else {
            c.close();
            return null;
        }
    }

    public List<Exercise> getAll() {
        Cursor c = mDatabase.query(ExerciseTable.TABLE_NAME, ExerciseTable.ALL_COLUMNS, null, null, null, null, null);
        c.moveToFirst();

        List<Exercise> exercises = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                exercises.add(cursorToExercise(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return exercises;
    }

    public List<Exercise> getAllByCampaign(int campaignId) {
        String where = String.format("%s = %d", ExerciseTable.Column.CAMPAIGN_ID, campaignId);
        Cursor c = mDatabase.query(ExerciseTable.TABLE_NAME, ExerciseTable.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        List<Exercise> exercises = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                exercises.add(cursorToExercise(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return exercises;
    }

    public List<Exercise> getAllByPerson(int personId) {
        String where = String.format("%s = %d", ExerciseTable.Column.PERSON_ID, personId);
        Cursor c = mDatabase.query(ExerciseTable.TABLE_NAME, ExerciseTable.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        List<Exercise> exercises = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                exercises.add(cursorToExercise(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return exercises;
    }

    public void delete(int id) {
        String where = String.format("%s = %d", ExerciseTable.Column.EXERCISE_ID, id);
        mDatabase.delete(ExerciseTable.TABLE_NAME, where, null);

        String whereSession = String.format("%s = %d", SessionsTable.Column.EXERCISE_ID, id);
        mDatabase.delete(SessionsTable.TABLE_NAME, whereSession, null);
    }
}
