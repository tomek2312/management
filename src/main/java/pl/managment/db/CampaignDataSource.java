package pl.managment.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Exercise;
import pl.managment.db.enitity.Tag;
import pl.managment.db.table.CampaignTable;

/**
 * @author Tomasz Trybała
 */
public class CampaignDataSource {
    private static CampaignDataSource sDataSource;
    private SQLiteDatabase mDatabase;

    private CampaignDataSource() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance();
        mDatabase = dbHelper.getDatabase();
    }

    private Campaign cursorToCampaign(Cursor c) {
        int id, clientId, personId;
        String name, description, endDate, startDate, avatar;

        id = c.getInt(CampaignTable.ColumnID.CAMPAIGN_ID);
        clientId = c.getInt(CampaignTable.ColumnID.CLIENT_ID);
        name = c.getString(CampaignTable.ColumnID.NAME);
        description = c.getString(CampaignTable.ColumnID.DESCRIPTION);
        endDate = c.getString(CampaignTable.ColumnID.END_DATE);
        startDate = c.getString(CampaignTable.ColumnID.START_DATE);
        avatar = c.getString(CampaignTable.ColumnID.AVATAR);
        personId = c.getInt(CampaignTable.ColumnID.PERSON_ID);

        return new Campaign(id, name, description, endDate, startDate, avatar, clientId, personId);
    }

    public static CampaignDataSource getInstance() {
        if (sDataSource == null) {
            sDataSource = new CampaignDataSource();
        }

        return sDataSource;
    }

    public Campaign get(int campaignId) {
        String where = String.format("%s = %d", CampaignTable.Column.CAMPAIGN_ID, campaignId);
        Cursor c = mDatabase.query(CampaignTable.TABLE_NAME, CampaignTable.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        if (c.getCount() > 0) {
            Campaign curiosity = cursorToCampaign(c);
            c.close();

            return curiosity;
        } else {
            c.close();
            return null;
        }
    }

    public List<Campaign> getAll() {
        Cursor c = mDatabase.query(CampaignTable.TABLE_NAME, CampaignTable.ALL_COLUMNS, null, null, null, null, null);
        c.moveToFirst();

        List<Campaign> campaigns = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                campaigns.add(cursorToCampaign(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return campaigns;
    }

    public void insertWithTags(Campaign campaign, boolean isId, ArrayList<String> tags) {
        ContentValues values = new ContentValues();
        if (isId) {
            values.put(CampaignTable.Column.CAMPAIGN_ID, campaign.getId());
        }
        values.put(CampaignTable.Column.AVATAR, campaign.getAvatar());
        values.put(CampaignTable.Column.NAME, campaign.getName());
        values.put(CampaignTable.Column.DESCRIPTION, campaign.getDescription());
        values.put(CampaignTable.Column.CLIENT_ID, campaign.getClientId());
        values.put(CampaignTable.Column.START_DATE, campaign.getStartDate());
        values.put(CampaignTable.Column.END_DATE, campaign.getEndDate());
        values.put(CampaignTable.Column.PERSON_ID, campaign.getPersonId());


        long id = mDatabase.insertWithOnConflict(CampaignTable.TABLE_NAME, null, values,
                SQLiteDatabase.CONFLICT_REPLACE);

        TagDataSource tds = TagDataSource.getInstance();
        for (String tag : tags) {
            Tag newTag = new Tag();
            newTag.setName(tag);
            newTag.setCampaignId((int) id);
            tds.insert(newTag);
        }
    }

    public void delete(int id) {
        String where = String.format("%s = %d", CampaignTable.Column.CAMPAIGN_ID, id);
        mDatabase.delete(CampaignTable.TABLE_NAME, where, null);

        ExerciseDataSource eds = ExerciseDataSource.getInstance();
        List<Exercise> exercises = eds.getAllByCampaign(id);
        for (Exercise exercise : exercises) {
            eds.delete(exercise.getId());
        }

        TagDataSource tds = TagDataSource.getInstance();
        tds.deleteByCampaign(id);
    }
}
