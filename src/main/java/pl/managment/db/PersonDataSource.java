package pl.managment.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

import pl.managment.db.enitity.Exercise;
import pl.managment.db.enitity.Person;
import pl.managment.db.enums.PersonFunction;
import pl.managment.db.table.PersonTable;

/**
 * @author Tomasz Trybała
 */
public class PersonDataSource {
    private static PersonDataSource sDataSource;
    private SQLiteDatabase mDatabase;

    private PersonDataSource() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance();
        mDatabase = dbHelper.getDatabase();
    }

    private Person cursorToPerson(Cursor c) {
        int id;
        String function, name, surname, dateOfBirth, mail, phone;
        String gender, address, avatar, password;

        id = c.getInt(PersonTable.ColumnID.PERSON_ID);
        function = c.getString(PersonTable.ColumnID.FUNCTION);
        name = c.getString(PersonTable.ColumnID.NAME);
        surname = c.getString(PersonTable.ColumnID.SURNAME);
        dateOfBirth = c.getString(PersonTable.ColumnID.DATE_OF_BIRTH);
        mail = c.getString(PersonTable.ColumnID.MAIL);
        phone = c.getString(PersonTable.ColumnID.PHONE);
        gender = c.getString(PersonTable.ColumnID.GENDER);
        address = c.getString(PersonTable.ColumnID.ADDRESS);
        avatar = c.getString(PersonTable.ColumnID.AVATAR);
        password = c.getString(PersonTable.ColumnID.PASSWORD);

        return new Person(id, function, name, surname, dateOfBirth, mail, phone, gender, address,
                avatar, password);
    }

    public static PersonDataSource getInstance() {
        if (sDataSource == null) {
            sDataSource = new PersonDataSource();
        }

        return sDataSource;
    }

    public Person get(int personId) {
        String where = String.format("%s = %d", PersonTable.Column.PERSON_ID, personId);
        Cursor c = mDatabase.query(PersonTable.TABLE_NAME, PersonTable.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        if (c.getCount() > 0) {
            Person curiosity = cursorToPerson(c);
            c.close();

            return curiosity;
        } else {
            c.close();
            return null;
        }
    }

    public List<Person> getAll() {
        Cursor c = mDatabase.query(PersonTable.TABLE_NAME, PersonTable.ALL_COLUMNS, null, null, null, null, null);
        c.moveToFirst();

        List<Person> people = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                people.add(cursorToPerson(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return people;
    }

    public List<Person> getAllManagers() {
        Cursor c = mDatabase.query(PersonTable.TABLE_NAME, PersonTable.ALL_COLUMNS, null, null, null, null, null);
        c.moveToFirst();

        List<Person> people = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                Person person = cursorToPerson(c);
                if (person.getFunction() != PersonFunction.WORKER) {
                    people.add(person);
                }
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return people;
    }

    public void insert(Person person, boolean isId) {
        ContentValues values = new ContentValues();
        if (isId) {
            values.put(PersonTable.Column.PERSON_ID, person.getId());
        }
        values.put(PersonTable.Column.ADDRESS, person.getAddress());
        values.put(PersonTable.Column.AVATAR, person.getAvatar());
        values.put(PersonTable.Column.DATE_OF_BIRTH, person.getDateBirth());
        values.put(PersonTable.Column.FUNCTION, person.getFunction().getName());
        values.put(PersonTable.Column.GENDER, person.getGender().getName());
        values.put(PersonTable.Column.MAIL, person.getMail());
        values.put(PersonTable.Column.NAME, person.getName());
        values.put(PersonTable.Column.PASSWORD, person.getPassword());
        values.put(PersonTable.Column.SURNAME, person.getSurname());
        values.put(PersonTable.Column.PHONE, person.getPhone());


        mDatabase.insertWithOnConflict(PersonTable.TABLE_NAME, null, values,
                SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void delete(int id) {
        String where = String.format("%s = %d", PersonTable.Column.PERSON_ID, id);
        mDatabase.delete(PersonTable.TABLE_NAME, where, null);

        ExerciseDataSource eds = ExerciseDataSource.getInstance();
        List<Exercise> exercises = eds.getAllByPerson(id);
        for (Exercise exercise : exercises) {
            exercise.setPersonId(-1);
            eds.insert(exercise, true);
        }
    }
}
