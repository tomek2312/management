package pl.managment.db.enitity;

import pl.managment.db.enums.ExercisePriority;
import pl.managment.db.enums.ExerciseStatus;

/**
 * @author Tomasz Trybała
 */
public class Exercise {
    private int mId;
    private String mName;
    private String mDescription;
    private int mPersonId;
    private String mStatus;
    private String mApproximateTime;
    private int mProgress;
    private String mPriority;
    private int mCampaignId;
    private String mCreated;

    public Exercise() {
    }

    public Exercise(int mId, String mName, String mDescription,
                    int mPersonId, String mStatus, String mApproximateTime,
                    int mProgress, String mPriority, int campaignId, String created) {
        this.mId = mId;
        this.mName = mName;
        this.mDescription = mDescription;
        this.mPersonId = mPersonId;
        this.mStatus = mStatus;
        this.mApproximateTime = mApproximateTime;
        this.mProgress = mProgress;
        this.mPriority = mPriority;
        this.mCampaignId = campaignId;
        this.mCreated = created;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getPersonId() {
        return mPersonId;
    }

    public void setPersonId(int mPersonId) {
        this.mPersonId = mPersonId;
    }

    public ExerciseStatus getStatus() {
        if(mStatus.equals(ExerciseStatus.CREATE.getName())) {
            return ExerciseStatus.CREATE;
        }else if(mStatus.equals(ExerciseStatus.IN_PROGRESS.getName())) {
            return ExerciseStatus.IN_PROGRESS;
        }else{
            return ExerciseStatus.CLOSED;
        }
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getAproximateTime() {
        return mApproximateTime;
    }

    public void setAproximateTime(String mAproximateTime) {
        this.mApproximateTime = mAproximateTime;
    }

    public int getProgress() {
        return mProgress;
    }

    public void setProgress(int mProgress) {
        this.mProgress = mProgress;
    }

    public ExercisePriority getPriority() {
        if(mPriority.equals(ExercisePriority.LOW.getName())) {
            return ExercisePriority.LOW;
        }else if(mPriority.equals(ExercisePriority.NORMAL.getName())) {
            return ExercisePriority.NORMAL;
        }else{
            return ExercisePriority.HIGH;
        }
    }

    public void setPriority(String mPriority) {
        this.mPriority = mPriority;
    }

    public int getCampaignId() {
        return mCampaignId;
    }

    public void setCampaignId(int mCampaignId) {
        this.mCampaignId = mCampaignId;
    }

    public String getCreated() {
        return mCreated;
    }

    public void setCreated(String mCreated) {
        this.mCreated = mCreated;
    }
}
