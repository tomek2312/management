package pl.managment.db.enitity;

/**
 *@author Tomasz Trybała
 */
public class Tag {
    private int mId;
    private String mName;
    private int mCampaignId;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public Tag(){}

    public Tag(int mId, String mName, int campaignId) {
        this.mId = mId;
        this.mName = mName;
        this.mCampaignId = campaignId;
    }

    public int getCampaignId() {
        return mCampaignId;
    }

    public void setCampaignId(int mCampaignId) {
        this.mCampaignId = mCampaignId;
    }
}
