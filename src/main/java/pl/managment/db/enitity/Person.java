package pl.managment.db.enitity;

import pl.managment.db.enums.PersonFunction;
import pl.managment.db.enums.PersonGender;

/**
 * @author Tomasz Trybała
 */
public class Person {
    private int mId;
    private String mFunction;
    private String mName;
    private String mSurname;
    private String mDateBirth;
    private String mMail;
    private String mPhone;
    private String mGender;
    private String mAddress;
    private String mAvatar;
    private String mPassword;

    public Person(){}

    public Person(int mId, String mFunction, String mName,
                  String mSurname, String mDateBirth, String mMail,
                  String mPhone, String mGender, String mAddress, String mAvatar,
                  String password) {
        this.mId = mId;
        this.mFunction = mFunction;
        this.mName = mName;
        this.mSurname = mSurname;
        this.mDateBirth = mDateBirth;
        this.mMail = mMail;
        this.mPhone = mPhone;
        this.mGender = mGender;
        this.mAddress = mAddress;
        this.mAvatar = mAvatar;
        this.mPassword = password;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public PersonFunction getFunction() {
        if (mFunction.equals(PersonFunction.ADMIN.getName())) {
            return PersonFunction.ADMIN;
        } else if (mFunction.equals(PersonFunction.MANAGER.getName())) {
            return PersonFunction.MANAGER;
        } else {
            return PersonFunction.WORKER;
        }
    }

    public void setFunction(String mFunction) {
        this.mFunction = mFunction;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSurname() {
        return mSurname;
    }

    public void setSurname(String mSurname) {
        this.mSurname = mSurname;
    }

    public String getDateBirth() {
        return mDateBirth;
    }

    public void setDateBirth(String mDateBirth) {
        this.mDateBirth = mDateBirth;
    }

    public String getMail() {
        return mMail;
    }

    public void setMail(String mMail) {
        this.mMail = mMail;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public PersonGender getGender() {
        if (mGender.equals(PersonGender.MALE.getName())) {
            return PersonGender.MALE;
        } else {
            return PersonGender.FEMALE;
        }
    }

    public void setGender(String mGender) {
        this.mGender = mGender;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String mAvatar) {
        this.mAvatar = mAvatar;
    }
}
