package pl.managment.db.enitity;

/**
 * @author Tomasz Trybała
 */
public class Campaign {
    private int mId;
    private String mName;
    private String mEndDate;
    private String mStartDate;
    private String mAvatar;
    private String mDescription;
    private int mClientId;
    private int mPersonId;

    public Campaign(){}

    public Campaign(int mId, String mName, String mDescription, String mEndDate,
                    String mStartDate, String mAvatar, int mClientId, int personId) {
        this.mId = mId;
        this.mName = mName;
        this.mDescription  = mDescription;
        this.mEndDate = mEndDate;
        this.mStartDate = mStartDate;
        this.mAvatar = mAvatar;
        this.mClientId = mClientId;
        this.mPersonId = personId;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public void setEndDate(String mEndDate) {
        this.mEndDate = mEndDate;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String mStartDate) {
        this.mStartDate = mStartDate;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String mAvatar) {
        this.mAvatar = mAvatar;
    }

    public int getClientId() {
        return mClientId;
    }

    public void setClientId(int mClientId) {
        this.mClientId = mClientId;
    }

    public int getPersonId() {
        return mPersonId;
    }

    public void setPersonId(int mPersonId) {
        this.mPersonId = mPersonId;
    }
}
