package pl.managment.db.enitity;

/**
 *@author Tomasz Trybała
 */
public class Client {
    private int mId;
    private String mName;
    private String mPhone;
    private String mPerson;

    public Client(){}

    public Client(int mId, String mName, String mPhone, String mPerson) {
        this.mId = mId;
        this.mName = mName;
        this.mPhone = mPhone;
        this.mPerson = mPerson;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getPerson() {
        return mPerson;
    }

    public void setPerson(String mPerson) {
        this.mPerson = mPerson;
    }
}
