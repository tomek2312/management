package pl.managment.db.enitity;

/**
 *@author Tomasz Trybała
 */
public class Session {
    private int mId;
    private int mExerciseId;
    private String mStart;
    private String mStop;
    private String mDescription;

    public Session(){}

    public Session(int mId, int mExerciseId, String mStart, String mStop, String mDescription) {
        this.mId = mId;
        this.mExerciseId = mExerciseId;
        this.mStart = mStart;
        this.mStop = mStop;
        this.mDescription = mDescription;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public int getExerciseId() {
        return mExerciseId;
    }

    public void setExerciseId(int mExerciseId) {
        this.mExerciseId = mExerciseId;
    }

    public String getStart() {
        return mStart;
    }

    public void setStart(String mStart) {
        this.mStart = mStart;
    }

    public String getStop() {
        return mStop;
    }

    public void setStop(String mStop) {
        this.mStop = mStop;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }
}
