package pl.managment.db.table;

/**
 * @author Tomasz Trybała
 */
public class ClientTable {
    private ClientTable() {
    }

    public static final String TABLE_NAME = "client";

    public static final class Column {
        public static final String CLIENT_ID = "id";
        public static final String NAME = "name";
        public static final String PHONE = "phone";
        public static final String PERSON = "person";
    }

    public static final class ColumnID {
        public static final int CLIENT_ID = 0;
        public static final int NAME = 1;
        public static final int PHONE = 2;
        public static final int PERSON = 3;
    }

    public static final String[] ALL_COLUMNS = {
            Column.CLIENT_ID,
            Column.NAME,
            Column.PHONE,
            Column.PERSON,
    };
}
