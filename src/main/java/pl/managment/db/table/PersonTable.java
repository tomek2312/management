package pl.managment.db.table;

/**
 *@author Tomasz Trybała
 */
public class PersonTable {
    private PersonTable() {
    }

    public static final String TABLE_NAME = "person";

    public static final class Column {
        public static final String PERSON_ID = "id";
        public static final String FUNCTION = "function";
        public static final String NAME = "name";
        public static final String SURNAME = "surname";
        public static final String DATE_OF_BIRTH = "date_of_birth";
        public static final String MAIL = "mail";
        public static final String PHONE = "phone";
        public static final String GENDER = "gender";
        public static final String ADDRESS = "address";
        public static final String AVATAR = "avatar";
        public static final String PASSWORD = "password";
    }

    public static final class ColumnID {
        public static final int PERSON_ID = 0;
        public static final int FUNCTION = 1;
        public static final int NAME = 2;
        public static final int SURNAME = 3;
        public static final int DATE_OF_BIRTH = 4;
        public static final int MAIL = 5;
        public static final int PHONE = 6;
        public static final int GENDER = 7;
        public static final int ADDRESS = 8;
        public static final int AVATAR = 9;
        public static final int PASSWORD = 10;
    }

    public static final String[] ALL_COLUMNS = {
            Column.PERSON_ID,
            Column.FUNCTION,
            Column.NAME,
            Column.SURNAME,
            Column.DATE_OF_BIRTH,
            Column.MAIL,
            Column.PHONE,
            Column.GENDER,
            Column.ADDRESS,
            Column.AVATAR,
            Column.PASSWORD
    };
}
