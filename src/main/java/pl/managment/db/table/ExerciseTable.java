package pl.managment.db.table;

/**
 *@author Tomasz Trybała
 */
public class ExerciseTable {
    private ExerciseTable() {
    }

    public static final String TABLE_NAME = "exercise";

    public static final class Column {
        public static final String EXERCISE_ID = "id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String STATUS = "status";
        public static final String TIME = "time";
        public static final String PROGRESS = "progress";
        public static final String PRIORITY = "priority";
        public static final String PERSON_ID = "personId";
        public static final String CAMPAIGN_ID = "campaignId";
        public static final String CREATED = "created";
    }

    public static final class ColumnID {
        public static final int EXERCISE_ID = 0;
        public static final int NAME = 1;
        public static final int DESCRIPTION = 2;
        public static final int STATUS = 3;
        public static final int TIME = 4;
        public static final int PROGRESS = 5;
        public static final int PRIORITY = 6;
        public static final int PERSON_ID = 7;
        public static final int CAMPAIGN_ID = 8;
        public static final int CREATED = 9;
    }

    public static final String[] ALL_COLUMNS = {
            Column.EXERCISE_ID,
            Column.NAME,
            Column.DESCRIPTION,
            Column.STATUS,
            Column.TIME,
            Column.PROGRESS,
            Column.PRIORITY,
            Column.PERSON_ID,
            Column.CAMPAIGN_ID,
            Column.CREATED
    };
}
