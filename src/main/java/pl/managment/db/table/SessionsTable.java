package pl.managment.db.table;

/**
 *@author Tomasz Trybała
 */
public class SessionsTable {
    private SessionsTable() {
    }

    public static final String TABLE_NAME = "sessions";

    public static final class Column {
        public static final String SESSION_ID = "id";
        public static final String EXERCISE_ID = "exerciseId";
        public static final String START = "start";
        public static final String STOP = "stop";
        public static final String DESCRIPTION = "description";
    }

    public static final class ColumnID {
        public static final int SESSION_ID = 0;
        public static final int EXERCISE_ID = 1;
        public static final int START = 2;
        public static final int STOP = 3;
        public static final int DESCRIPTION = 4;
    }

    public static final String[] ALL_COLUMNS = {
            Column.SESSION_ID,
            Column.EXERCISE_ID,
            Column.START,
            Column.STOP,
            Column.DESCRIPTION,
    };
}
