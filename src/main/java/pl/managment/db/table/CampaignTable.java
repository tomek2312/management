package pl.managment.db.table;

/**
 *@author Tomasz Trybała
 */
public class CampaignTable {
    private CampaignTable() {
    }

    public static final String TABLE_NAME = "campaign";

    public static final class Column {
        public static final String CAMPAIGN_ID = "id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String CLIENT_ID = "clientId";
        public static final String START_DATE = "start_date";
        public static final String AVATAR = "avatar";
        public static final String END_DATE = "end_date";
        public static final String PERSON_ID = "personId";
    }

    public static final class ColumnID {
        public static final int CAMPAIGN_ID = 0;
        public static final int NAME = 1;
        public static final int DESCRIPTION = 2;
        public static final int CLIENT_ID = 3;
        public static final int START_DATE = 4;
        public static final int AVATAR = 5;
        public static final int END_DATE = 6;
        public static final int PERSON_ID = 7;
    }

    public static final String[] ALL_COLUMNS = {
            Column.CAMPAIGN_ID,
            Column.NAME,
            Column.DESCRIPTION,
            Column.CLIENT_ID,
            Column.START_DATE,
            Column.AVATAR,
            Column.END_DATE,
            Column.PERSON_ID
    };
}
