package pl.managment.db.table;

/**
 *@author Tomasz Trybała
 */
public class TagTable {
    private TagTable() {
    }

    public static final String TABLE_NAME = "tags";

    public static final class Column {
        public static final String TAG_ID = "id";
        public static final String TAG = "tag";
        public static final String CAMPAIGN_ID = "campaignId";
    }

    public static final class ColumnID {
        public static final int TAG_ID = 0;
        public static final int TAG = 1;
        public static final int CAMPAIGN_ID = 2;
    }

    public static final String[] ALL_COLUMNS = {
            Column.TAG_ID,
            Column.TAG,
            Column.CAMPAIGN_ID
    };
}
