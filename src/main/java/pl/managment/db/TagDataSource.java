package pl.managment.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

import pl.managment.db.enitity.Tag;
import pl.managment.db.table.TagTable;

/**
 * @author Tomasz Trybała
 */
public class TagDataSource {
    private static TagDataSource sDataSource;
    private SQLiteDatabase mDatabase;

    private TagDataSource() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance();
        mDatabase = dbHelper.getDatabase();
    }

    private Tag cursorToTag(Cursor c) {
        int id, campaignId;
        String tag;

        id = c.getInt(TagTable.ColumnID.TAG_ID);
        tag = c.getString(TagTable.ColumnID.TAG);
        campaignId = c.getInt(TagTable.ColumnID.CAMPAIGN_ID);

        return new Tag(id, tag, campaignId);
    }

    public static TagDataSource getInstance() {
        if (sDataSource == null) {
            sDataSource = new TagDataSource();
        }

        return sDataSource;
    }

    public Tag get(int tagId) {
        String where = String.format("%s = %d", TagTable.Column.TAG_ID, tagId);
        Cursor c = mDatabase.query(TagTable.TABLE_NAME, TagTable.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        if (c.getCount() > 0) {
            Tag ta = cursorToTag(c);
            c.close();

            return ta;
        } else {
            c.close();
            return null;
        }
    }

    public List<Tag> getAll() {
        Cursor c = mDatabase.query(TagTable.TABLE_NAME, TagTable.ALL_COLUMNS, null, null, null, null, null);
        c.moveToFirst();

        List<Tag> tags = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                tags.add(cursorToTag(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return tags;
    }

    public List<Tag> getAllByCampaign(int campaignId) {
        String where = String.format("%s = %d", TagTable.Column.CAMPAIGN_ID, campaignId);
        Cursor c = mDatabase.query(TagTable.TABLE_NAME, TagTable.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        List<Tag> tags = new LinkedList<>();

        if (c.getCount() > 0) {
            do {
                tags.add(cursorToTag(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return tags;
    }

    public void insert(Tag tag) {
        ContentValues values = new ContentValues();
        values.put(TagTable.Column.TAG, tag.getName());
        values.put(TagTable.Column.CAMPAIGN_ID, tag.getCampaignId());

        mDatabase.insertWithOnConflict(TagTable.TABLE_NAME, null, values,
                SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void deleteById(int id) {
        String where = String.format("%s = %d", TagTable.Column.TAG_ID, id);
        mDatabase.delete(TagTable.TABLE_NAME, where, null);
    }

    public void deleteByNameAndCampaign(int campaignId, String name) {
        String where = String.format("%s = %d AND %s = %s",
                TagTable.Column.CAMPAIGN_ID, campaignId,
                TagTable.Column.TAG, name);
        mDatabase.delete(TagTable.TABLE_NAME, where, null);
    }

    public void deleteByCampaign(int campaignId) {
        String where = String.format("%s = %d", TagTable.Column.CAMPAIGN_ID, campaignId);
        mDatabase.delete(TagTable.TABLE_NAME, where, null);
    }
}
