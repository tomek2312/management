package pl.managment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Environment;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.util.LruCache;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybała
 */

@Singleton
public final class ImageUtils {
    private static final int BITMAP_MAX_WIDTH = 1080;
    private static final int BITMAP_MAX_HEIGHT = 1920;
    public static final int IMAGE_DEFAULT_SAVE_SIZE = 512;

    protected static LruCache<String, Bitmap> sImageCache;

    static {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory());
        final int cacheSize = maxMemory / 4;

        sImageCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getRowBytes() * value.getHeight();
            }
        };
    }

    @Inject
    public ImageUtils() {
    }

    public void clearCache() {
        sImageCache.evictAll();
    }

    /**
     * Retrieves specified bitmap from the cache and returns it.
     *
     * @param key bitmap key, cannot be null or empty
     * @return Bitmap object if there's a bitmap with specified key in the cache or null otherwise
     */
    public
    @Nullable
    Bitmap getFromCache(@NonNull String key) {
        checkNotNull(key, "key cannot be null");
        checkArgument(!key.isEmpty(), "key cannot be empty");

        return sImageCache.get(key);
    }

    /**
     * Adds a bitmap to the cache.
     *
     * @param key bitmap key, cannot be null or empty
     * @param bmp bitmap object, cannot be null
     */
    public void addToCache(@NonNull String key, @NonNull Bitmap bmp) {
        checkNotNull(key, "key cannot be null");
        checkNotNull(bmp, "bmp cannot be null");

        if (getFromCache(key) == null) {
            sImageCache.put(key, bmp);
        }
    }

    /**
     * Checks whether the key is present in the image cache.
     *
     * @param key the key that should be checked
     * @return true if the key is present, false otherwise
     */
    public boolean isPresentInCache(@NonNull String key) {
        checkNotNull(key, "kay cannot be null");
        checkArgument(!key.isEmpty(), "key cannot be empty");

        return sImageCache.get(key) != null;
    }

    public
    @NonNull
    Bitmap transformToCircle(@NonNull Bitmap bmp) {
        checkNotNull(bmp, "bmp cannot be null");
        int size = (int) MyApplication.getAppContext().getResources().
                getDimension(R.dimen.dashboard_image_big);

        Bitmap output = Bitmap.createBitmap(size,
                size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, size,
                size);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(size / 2,
                size / 2, size / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bmp, rect, rect, paint);
        return output;
    }

    public
    @NonNull
    Bitmap transformToOval(@NonNull Context context, @NonNull Bitmap bmp, int borderColor, int drawable) {
        checkNotNull(bmp, "bmp cannot be null");

        Paint p = new Paint();
        p.setColor(0xff000000);
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setAntiAlias(true);
        p.setDither(true);
        p.setFilterBitmap(true);

        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int bmpX, bmpY, bmpWidth, bmpHeight;

        if (w > h) {
            bmpX = (w - h) / 2;
            bmpY = 0;
            bmpHeight = h;
            bmpWidth = h;
        } else {
            bmpX = 0;
            bmpWidth = w;
            bmpY = (h - w) / 2;
            bmpHeight = w;
        }

        int borderWidth = (int) (0.03f * bmpWidth);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

//        BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_profile_mask, options);
        options.inSampleSize = calculateInSampleSize(options, bmpWidth, bmpHeight);
        options.inJustDecodeBounds = false;

        Bitmap mask = BitmapFactory.decodeResource(context.getResources(), drawable, options);
        if (mask == null) {
            return bmp;
        }

        Bitmap newMask = Bitmap.createScaledBitmap(mask, bmpWidth, bmpHeight, true);
        Bitmap circleImage = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(circleImage);
        c.drawBitmap(newMask, bmpX, bmpY, p);
        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        c.drawBitmap(bmp, 0.0f, 0.0f, p);

        Bitmap resultImage = Bitmap.createBitmap(w + borderWidth, h + borderWidth, Bitmap.Config.ARGB_8888);

        newMask.recycle();

        Bitmap finalBitmap = Bitmap.createScaledBitmap(mask, bmpWidth + borderWidth, bmpHeight + borderWidth, true);
        c = new Canvas(finalBitmap);
        c.drawColor(borderColor, PorterDuff.Mode.SRC_IN);

        c = new Canvas(resultImage);
        c.drawBitmap(finalBitmap, bmpX, bmpY, new Paint());
        c.drawBitmap(circleImage, borderWidth / 2, borderWidth / 2, new Paint());

        mask.recycle();
        finalBitmap.recycle();

        return resultImage;
    }

    public
    @NonNull
    Bitmap transformToOval(@NonNull Context context, @NonNull Bitmap bmp, int drawable) {
        return transformToOval(context, bmp, 0xffffffff, drawable);
    }

    public static int calculateInSampleSize(@NonNull BitmapFactory.Options options, int reqWidth, int reqHeight) {
        checkNotNull(options, "BitmapFactory options cannot be null");
        checkArgument(reqWidth > 0, "reqWidth has to be greater than 0");
        checkArgument(reqHeight > 0, "reqHeight has to be greater than 0");

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            //todo zmienione && na || wrazie czego odmienic
            while ((halfHeight / inSampleSize) > reqHeight
                    || (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public void saveBitmapToFile(@NonNull Bitmap bmp, @NonNull File outputFile) throws IOException {
        checkNotNull(bmp, "Bitmap cannot be null");
        checkNotNull(outputFile, "Output file cannot be null");

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 0, bos);
        byte[] bitmapdata = bos.toByteArray();

        FileOutputStream fos = new FileOutputStream(outputFile);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
    }

    public
    @NonNull
    Optional<Bitmap> loadPreScaledBitmapMax(@NonNull File file) throws IOException {
        return loadPreScaledBitmap(file, BITMAP_MAX_WIDTH, BITMAP_MAX_HEIGHT);
    }

    public
    @NonNull
    Optional<Bitmap> loadPreScaledBitmap(@NonNull File file, int requiredWidth,
                                         int requiredHeight) throws IOException {
        checkNotNull(file, "File cannot be null");
        checkArgument(requiredWidth > 0, "reqWidth has to be greater than 0");
        checkArgument(requiredHeight > 0, "reqHeight has to be greater than 0");

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) &&
                !Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            throw new IOException("external storage not available for reading");
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        options.inSampleSize = calculateInSampleSize(options, requiredWidth, requiredHeight);
        options.inJustDecodeBounds = false;

        return Optional.fromNullable(BitmapFactory.decodeFile(file.getAbsolutePath(), options));
    }

    public
    @NonNull
    Optional<Bitmap> loadPreScaledBitmap(@DrawableRes int drawable, int requiredWidth,
                                         int requiredHeight) throws IOException {
        checkArgument(requiredWidth > 0, "reqWidth has to be greater than 0");
        checkArgument(requiredHeight > 0, "reqHeight has to be greater than 0");

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) &&
                !Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            throw new IOException("external storage not available for reading");
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeResource(MyApplication.getAppContext().getResources(),
                drawable, options);
        options.inSampleSize = calculateInSampleSize(options, requiredWidth, requiredHeight);
        options.inJustDecodeBounds = false;

        return Optional.fromNullable(BitmapFactory.decodeResource(MyApplication.getAppContext().getResources(),
                drawable, options));
    }

    public
    @NonNull
    Bitmap blurImage(@NonNull Context context, @NonNull Bitmap input, int radius) {
        checkNotNull(context, "context cannot be null");
        checkNotNull(input, "input cannot be null");
        checkArgument(radius > 0, "radius has to be greater than 0");
        checkArgument(radius <= 25, "radius has to be smaller than 25");

        RenderScript rsScript = RenderScript.create(context);
        Allocation alloc = Allocation.createFromBitmap(rsScript, input);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(rsScript, alloc.getElement());
        blur.setRadius(radius);
        blur.setInput(alloc);

        Bitmap result = Bitmap.createBitmap(input.getWidth(), input.getHeight(), input.getConfig());
        Allocation outAlloc = Allocation.createFromBitmap(rsScript, result);
        blur.forEach(outAlloc);
        outAlloc.copyTo(result);

        rsScript.destroy();
        return result;
    }

    public static Bitmap decodeSampledBitmapFromResource(String filePath,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    public Bitmap getResizedBitmap(int targetW, int targetH,  String imagePath) {

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        //inJustDecodeBounds = true <-- will not load the bitmap into memory
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return(BitmapFactory.decodeFile(imagePath, bmOptions));
    }

    public
    @NonNull
    Bitmap getRoundedCornerBitmap(@NonNull Bitmap bitmap, int pixelOffset) {
        checkNotNull(bitmap, "bitmap cannot be null");
        checkArgument(pixelOffset >= 0, "pixelOffset cannot be less than 0");

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, pixelOffset, pixelOffset, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public
    @NonNull
    Bitmap getRoundedTaskBitmap(@NonNull Bitmap bitmap) {
        int padding = (int) MyApplication.getAppContext().getResources().
                getDimension(R.dimen.padding_default);
        return getRoundedCornerBitmap(cropBitmapToSquare(bitmap), padding);
    }

    public
    @NonNull
    Bitmap cropBitmapToSquare(@NonNull Bitmap srcBmp) {
        checkNotNull(srcBmp, "scrBmp cannot be null");

        Bitmap dstBmp;
        if (srcBmp.getWidth() >= srcBmp.getHeight()) {
            dstBmp = Bitmap.createBitmap(
                    srcBmp,
                    srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2,
                    0,
                    srcBmp.getHeight(),
                    srcBmp.getHeight()
            );

        } else {
            dstBmp = Bitmap.createBitmap(
                    srcBmp,
                    0,
                    srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2,
                    srcBmp.getWidth(),
                    srcBmp.getWidth()
            );
        }

        return dstBmp;
    }

    /**
     * Combines two given bitmaps using specified Porter-Duff Xfermode.
     *
     * @param src  source image (main image)
     * @param dst  destination image (puzzle image)
     * @param mode Xfermode
     * @return combined bitmap
     */
    public static Bitmap combineImages(Bitmap src, Bitmap dst, PorterDuff.Mode mode) {
        Bitmap output = Bitmap.createBitmap(dst.getWidth(), dst.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        Paint paint = new Paint();
        canvas.drawBitmap(src, 0, 0, paint);
        paint.setXfermode(new PorterDuffXfermode(mode));
        canvas.drawBitmap(dst, 0, 0, paint);

        return output;
    }

    public
    @NonNull
    Bitmap toGrayscale(@NonNull Bitmap bmpOriginal) {
        checkNotNull(bmpOriginal, "bmpOriginal cannot be null");

        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayScale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayScale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayScale;
    }
}