package pl.managment.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import pl.managment.ImageUtils;
import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Exercise;
import pl.managment.db.enitity.Person;
import pl.managment.fragments.TaskFragment;
import pl.managment.view.FillImageView;

/**
 * Pager adapter of view pager in dashboard list row.
 *
 * @author Tomasz Trybała
 */
public class TaskPagerAdapter extends PagerAdapter {
    private LayoutInflater mInflater;
    private MainActivity mActivity;
    private int mCount;
    private final ArrayList<Exercise> mData;
    protected HashMap<Integer, Campaign> mCampaigns;
    private HashMap<Integer, Person> mWorkers;

    private void loadPerson(ImageView imageView, TextView textView, int exerciseId) {
        if (mWorkers.containsKey(exerciseId)) {
            Person person = mWorkers.get(exerciseId);
            textView.setText(person.getName().substring(0, 1) + ". " + person.getSurname());
            try {
                loadAvatar(person.getAvatar(), imageView);
            } catch (IOException e) {
                imageView.setImageResource(R.drawable.male_avatar);
            }

        } else {
            textView.setText("---");
            imageView.setImageResource(R.drawable.male_avatar);
        }
    }

    private void loadAvatar(final String avatar, final ImageView imageView)
            throws IOException {
        AsyncTask<Void, Void, Void> loader = new AsyncTask<Void, Void, Void>() {
            private Bitmap mBitmap = BitmapFactory.decodeResource(
                    MyApplication.getAppContext().getResources(),
                    R.drawable.male_avatar);

            @Override
            protected void onPreExecute() {
                imageView.setVisibility(View.INVISIBLE);
            }

            @Override
            protected Void doInBackground(Void... params) {
                if (avatar != null && !TextUtils.isEmpty(avatar)) {
                    File tempImage = new File(avatar);
                    if (tempImage.exists()) {
                        int size = (int) MyApplication.getAppContext().getResources().
                                getDimension(R.dimen.dashboard_image_small);
                        ImageUtils utils = new ImageUtils();
                        //todo change *2
                        mBitmap = utils.getResizedBitmap(size * 4, size * 2, tempImage.getAbsolutePath());
//                            mBitmap = utils.getRoundedTaskBitmap(bitmap);
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                imageView.setImageBitmap(mBitmap);
                imageView.setVisibility(View.VISIBLE);
            }
        };

        loader.execute();
    }

    /**
     * Constructor.
     *
     * @param pagerItems list of items to display
     */
    public TaskPagerAdapter(
            MainActivity activity, ArrayList<Exercise> pagerItems, HashMap<Integer, Campaign> campaigns,
            HashMap<Integer, Person> people) {
        super();
        mActivity = activity;
        this.mData = pagerItems;
        mCampaigns = campaigns;
        mWorkers = people;
        mCount = mData.size();
        mInflater = LayoutInflater.from(activity);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        RelativeLayout layout = (RelativeLayout) mInflater.inflate(R.layout.pager_row_task, null);
        ImageView mPersonAvatar = (ImageView) layout.findViewById(R.id.imgvCampaignImage);
        TextView mCode = (TextView) layout.findViewById(R.id.txtvCampaignCode);
        FillImageView mProgress = (FillImageView) layout.findViewById(R.id.fivCampaignProgress);
        TextView mDescription = (TextView) layout.findViewById(R.id.txtvCampaignDescription);
        TextView mPerson = (TextView) layout.findViewById(R.id.txtvCampaignPerson);
        TextView mStatus = (TextView) layout.findViewById(R.id.txtvCampaignStatus);
        ImageView imgvArrowLeft = (ImageView) layout.findViewById(R.id.imgvArrowLeft);
        ImageView imgvArrowRight = (ImageView) layout.findViewById(R.id.imgvArrowRight);

        Exercise exercise = mData.get(position);
        mProgress.animateView(exercise.getProgress());
        mDescription.setText(exercise.getDescription());
        mCode.setText(mCampaigns.get(exercise.getId()).getName().substring(0, 3).toUpperCase() + "-" + exercise.getId());
        mStatus.setText(exercise.getStatus().getDisplayName().toUpperCase());
        loadPerson(mPersonAvatar, mPerson, exercise.getId());
        imgvArrowLeft.setVisibility(position != 0 ? View.VISIBLE : View.INVISIBLE);
        imgvArrowRight.setVisibility(position != mData.size() - 1 ? View.VISIBLE : View.INVISIBLE);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Exercise exercise = mData.get(position);
                mActivity.setFragment(TaskFragment.getInstance(exercise,
                        mWorkers.get(exercise.getId()),
                        mCampaigns.get(exercise.getId())), true);
            }
        });

        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view.equals(obj);
    }
}