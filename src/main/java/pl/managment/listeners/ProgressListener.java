package pl.managment.listeners;

/**
 * @author Tomasz Trybała
 */
public interface ProgressListener {
    void onProgressChange(int value);
}
