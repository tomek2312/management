package pl.managment.listeners;

import pl.managment.db.enitity.Client;

/**
 *@author Tomasz Trybała
 */
public interface ClientListener {
    void onCreated(Client client);
}
