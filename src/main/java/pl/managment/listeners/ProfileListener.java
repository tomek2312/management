package pl.managment.listeners;

/**
 *@author Tomasz Trybała
 */
public interface ProfileListener {
    void onChange(String function, String dateOfBirth,
                  String address, String phone, String password);
}
