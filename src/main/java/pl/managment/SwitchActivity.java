package pl.managment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.common.base.Optional;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
@ContentView(R.layout.activity_switch)
public class SwitchActivity extends RoboEventCompatActivity {
    @InjectView(R.id.btnSwitchDrawer)
    private Button mBtnDrawer;

    @InjectView(R.id.btnSwitchMenu)
    private Button mBtnMenu;

    @InjectView(R.id.btnSwitchTabs)
    private Button mBtnTabs;

    private void setListeners() {
        mBtnDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(BundleConstants.EXTRA_DRAWER);
            }
        });

        mBtnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(BundleConstants.EXTRA_MENU);
            }
        });

        mBtnTabs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(BundleConstants.EXTRA_TABS);
            }
        });
    }

    private void startActivity(String extraString) {
        Intent intent = new Intent(SwitchActivity.this, MainActivity.class);
        intent.putExtra(extraString, "");
        startActivity(intent);
//        finish();
    }


    private void checkProfile() {
        Optional<String> profile = MyApplication.loadFromSettings(BundleConstants.LOGIN_PERSON_ID);
        if (profile.isPresent()) {
            setListeners();
        } else {
            Intent intent = new Intent(SwitchActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkProfile();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Optional<String> profile = MyApplication.loadFromSettings(BundleConstants.LOGIN_PERSON_ID);
        if (!profile.isPresent()) {
            Intent intent = new Intent(SwitchActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
