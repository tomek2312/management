package pl.managment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Standard json object request with custom timeout.
 *
 * @author Tomasz Trybała
 */
public class JsonRequest extends JsonObjectRequest {
    private static final int TIMEOUT = 15; //seconds

    public JsonRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        setTimeout();
    }

    private void setTimeout() {
        setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT * 1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}
