package pl.managment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Person;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
@ContentView(R.layout.activity_login)
public class LoginActivity extends RoboEventCompatActivity {
    @InjectView(R.id.edtLoginLogin)
    private EditText mEdtLogin;

    @InjectView(R.id.edtLoginPassword)
    private EditText mEdtPassword;

    @InjectView(R.id.btnLoginLogin)
    private Button mBtnLogin;

    @InjectView(R.id.imgvLoginVisibility)
    private ImageView mImgvVisibility;

    public ProgressDialog mDialog;
    private boolean mIsPasswordVisibility;

    /**
     * Method sets progress dialog, which is shown
     * when data from server is loading.
     */
    private void setProgressDialog() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Logowanie...");
        mDialog.setCancelable(false);
    }

    /**
     * Method allows user to show or hide his password.
     */
    private void changePasswordVisibility() {
        mIsPasswordVisibility = !mIsPasswordVisibility;
        int indexStart = mEdtPassword.getSelectionStart();
        int indexEnd = mEdtPassword.getSelectionEnd();

        if (mIsPasswordVisibility) {
            mImgvVisibility.setImageResource(R.drawable.ic_password_show);
            mEdtPassword.setTransformationMethod(null);
        } else {
            mImgvVisibility.setImageResource(R.drawable.ic_password_hide);
            mEdtPassword.setTransformationMethod(new PasswordTransformationMethod());
        }

        mEdtPassword.setSelection(indexStart, indexEnd);
    }

    private void checkData() {
        AsyncTask<Void, Void, Person> checker = new AsyncTask<Void, Void, Person>() {
            @Override
            protected void onPreExecute() {
                showDialog();
            }

            @Override
            protected Person doInBackground(Void... params) {
                PersonDataSource dataSource = PersonDataSource.getInstance();
                List<Person> people = dataSource.getAll();
                for (Person person : people) {
                    if (person.getMail().equals(mEdtLogin.getText().toString()) &&
                            person.getPassword().equals(mEdtPassword.getText().toString())) {
                        return person;
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Person person) {
                hideDialog();
                if (person == null) {
                    showToast("Nieprawidłowe dane!");
                } else {
                    MyApplication.saveToSettings(BundleConstants.LOGIN_PERSON_ID,
                            "" + person.getId());
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };

        checker.execute();
    }

    private void setListeners() {
        mImgvVisibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePasswordVisibility();
            }
        });

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkData();
            }
        });

        mEdtLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                setLoginButton(isFormValid());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mEdtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                setLoginButton(isFormValid());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mEdtLogin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    if (ValidationUtils.isValidEmail(mEdtLogin.getText().toString())) {
                        mEdtLogin.setError(null);
                    } else {
                        mEdtLogin.setError("Nieprawidłowy adres e-mail");
                    }
                }
            }
        });
    }

    /**
     * Method sets login button enabled
     * if form is valid or sets disabled
     * if form is invalid.
     *
     * @param isValidForm if form is valid
     */
    private void setLoginButton(boolean isValidForm) {
        if (isValidForm) {
            mBtnLogin.setEnabled(true);
        } else {
            mBtnLogin.setEnabled(false);
        }
    }

    /**
     * Method checks if mail pattern is valid
     * and if password is not empty.
     *
     * @return true     mail and password is valid
     * false    mail or passwords is invalid
     */
    private boolean isFormValid() {
        return ValidationUtils.isValidEmail(mEdtLogin.getText().toString()) &&
                ValidationUtils.isValidPassword(mEdtPassword.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setProgressDialog();
        setListeners();
    }

    /**
     * Method shows progress dialog.
     */
    public void showDialog() {
        mDialog.show();
    }

    /**
     * Method hides progress dialog.
     */
    public void hideDialog() {
        if (mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    /**
     * Method shows toast.
     *
     * @param message message to user.
     */
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
