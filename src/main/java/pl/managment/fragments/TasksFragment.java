package pl.managment.fragments;

import java.util.List;

import pl.managment.db.CampaignDataSource;
import pl.managment.db.ExerciseDataSource;
import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Exercise;
import pl.managment.db.enitity.Person;

/**
 * @author Tomasz Trybała
 */
public class TasksFragment extends CampaignTasksFragment {
    private int mPersonId;

    private void setId(int personId) {
        mPersonId = personId;
    }

    public static TasksFragment getInstance(int personId) {
        TasksFragment fragment = new TasksFragment();
        fragment.setId(personId);

        return fragment;
    }

    @Override
    protected void load() {
        ExerciseDataSource eds = ExerciseDataSource.getInstance();
        mTasks.addAll(eds.getAllByPerson(mPersonId));

        PersonDataSource pds = PersonDataSource.getInstance();
        List<Person> people = pds.getAll();

        CampaignDataSource cds = CampaignDataSource.getInstance();
        List<Campaign> campaigns = cds.getAll();

        for (Exercise exercise : mTasks) {
            for (Person person : people) {
                if (exercise.getPersonId() == person.getId()) {
                    mPeople.put(exercise.getId(), person);
                    break;
                }
            }

            for (Campaign campaign : campaigns) {
                if (exercise.getCampaignId() == campaign.getId()) {
                    mCampaigns.put(exercise.getId(), campaign);
                    break;
                }
            }
        }
    }
}
