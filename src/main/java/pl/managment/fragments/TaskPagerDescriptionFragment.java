package pl.managment.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pl.managment.R;
import pl.managment.db.enitity.Exercise;
import roboguice.inject.InjectView;

/**
 *@author Tomasz Trybała
 */
public class TaskPagerDescriptionFragment extends RoboEventFragment{
    @InjectView(R.id.txtvTaskDescription)
    private TextView mTxtvDescription;

    private Exercise mExercise;

    private void setData(Exercise exercise) {
        mExercise = exercise;
    }

    public static TaskPagerDescriptionFragment getInstance(Exercise exercise) {
        TaskPagerDescriptionFragment fragment = new TaskPagerDescriptionFragment();
        fragment.setData(exercise);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_description, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTxtvDescription.setText(mExercise.getDescription());
    }
}
