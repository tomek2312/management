package pl.managment.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.adapter.TaskPagerAdapter;
import pl.managment.db.CampaignDataSource;
import pl.managment.db.ExerciseDataSource;
import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Exercise;
import pl.managment.db.enitity.Person;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class CampaignTaskPagerFragment extends RoboEventFragment {
    @InjectView(R.id.viewPager)
    private ViewPager mViewPager;

    private ArrayList<Exercise> mData = new ArrayList<>();
    private HashMap<Integer, Campaign> mCampaigns = new HashMap<>();
    private HashMap<Integer, Person> mWorkers = new HashMap<>();
    private int mPersonId;
    private MainActivity mActivity;

    private void setData(int personId) {
        mPersonId = personId;
    }

    private void setPager() {
        mViewPager.setAdapter(new TaskPagerAdapter(mActivity, mData, mCampaigns, mWorkers));
    }

    private void load() {
        AsyncTask<Void, Void, Void> loader = new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                mData.clear();
            }

            @Override
            protected Void doInBackground(Void... params) {
                ExerciseDataSource eds = ExerciseDataSource.getInstance();
                mData.addAll(eds.getAllByPerson(mPersonId));

                PersonDataSource pds = PersonDataSource.getInstance();
                List<Person> people = pds.getAll();

                CampaignDataSource cds = CampaignDataSource.getInstance();
                List<Campaign> campaigns = cds.getAll();

                for (Exercise exercise : mData) {
                    for (Person person : people) {
                        if (exercise.getPersonId() == person.getId()) {
                            mWorkers.put(exercise.getId(), person);
                            break;
                        }
                    }

                    for (Campaign campaign : campaigns) {
                        if (exercise.getCampaignId() == campaign.getId()) {
                            mCampaigns.put(exercise.getId(), campaign);
                            break;
                        }
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                setPager();
            }
        };

        loader.execute();
    }

    public static CampaignTaskPagerFragment getInstance(int personId) {
        CampaignTaskPagerFragment fragment = new CampaignTaskPagerFragment();
        fragment.setData(personId);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_pager, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        load();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mActivity.getMenuInflater().inflate(R.menu.menu_main_task, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings_task) {
            Display display = mActivity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int y = size.y - mViewPager.getMeasuredHeight();
            int x = mViewPager.getRight() - (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 200,
                    MyApplication.getAppContext().getResources().getDisplayMetrics());

            Point point = new Point();
            point.x = x;
            point.y = y;
            setPopup(point);
            setPopup(point);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setPopup(Point p) {
        LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_menu_task, null);
        LinearLayout llProfile = (LinearLayout) layout.findViewById(R.id.llPopupProfile);
        LinearLayout llProjects = (LinearLayout) layout.findViewById(R.id.llPopupProjects);
        LinearLayout llTasks = (LinearLayout) layout.findViewById(R.id.llPopupTasks);
        LinearLayout llSettings = (LinearLayout) layout.findViewById(R.id.llPopupSettings);

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(mActivity);
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the left, and a bit down, relative to button's position.
        int offset = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8,
                MyApplication.getAppContext().getResources().getDisplayMetrics());

        //Clear the default translucent background
        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x - offset, p.y + offset);

        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        llProjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        llTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        llSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }
}
