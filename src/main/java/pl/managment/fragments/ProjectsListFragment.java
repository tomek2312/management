package pl.managment.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.common.base.Optional;
import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.managment.ImageUtils;
import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.CampaignDataSource;
import pl.managment.db.ClientDataSource;
import pl.managment.db.PersonDataSource;
import pl.managment.db.TagDataSource;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Client;
import pl.managment.db.enitity.Person;
import pl.managment.db.enitity.Tag;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class ProjectsListFragment extends RoboEventFragment {
    /*package*/ static class CampaignListAdapter extends ArrayAdapter<Campaign> {
        private final Context mContext;

        private final ArrayList<Campaign> mData;
        private HashMap<Integer, Client> mClients;
        private HashMap<Integer, List<Tag>> mStringTags;
        private final int mLayout;

        private class ViewHolder {
            public ImageView mImage;
            public TextView mClient;
            public TextView mTitle;
            public TextView mDescription;
            public TextView mStart;
            public TextView mEnd;
            public TextView mTags;
            public ProgressBar mProgress;
        }

        public CampaignListAdapter(Context context, ArrayList<Campaign> data, int layout) {
            super(context, layout, data);

            mContext = context;
            mData = data;
            mLayout = layout;
        }

        public void setClients(HashMap<Integer, Client> clients) {
            mClients = clients;
        }

        public void setTags(HashMap<Integer, List<Tag>> map) {
            mStringTags = map;
        }

        private void loadAvatar(final String avatar, final ImageView imageView, final ProgressBar progressBar)
                throws IOException {
            AsyncTask<Void, Void, Bitmap> loader = new AsyncTask<Void, Void, Bitmap>() {
                @Override
                protected void onPreExecute() {
                    progressBar.setVisibility(View.VISIBLE);
                    imageView.setVisibility(View.INVISIBLE);
                }

                @Override
                protected Bitmap doInBackground(Void... params) {
                    if (!TextUtils.isEmpty(avatar)) {
                        File tempImage = new File(avatar);
                        if (tempImage.exists()) {
                            Bitmap example = BitmapFactory.decodeResource(
                                    MyApplication.getAppContext().getResources(),
                                    R.drawable.campaign_placeholder);
                            ImageUtils imageUtils = new ImageUtils();
                            try {
                                Optional<Bitmap> optBitmap = imageUtils.loadPreScaledBitmap(tempImage,
                                        example.getWidth(), example.getHeight());
                                if (optBitmap.isPresent()) {
                                    return optBitmap.get();
                                }
                            } catch (IOException e) {
                                return null;
                            }
                        }
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    if (bitmap != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                    progressBar.setVisibility(View.GONE);
                    imageView.setVisibility(View.VISIBLE);
                }
            };

            loader.execute();
        }

        private void loadTags(TextView textView, int campaignId) {

            String start = "<font color='#ffffff'>Tagi:</font>";
            String tagStart = "<font color='#e91e63'>";

            if (mStringTags != null && mStringTags.containsKey(campaignId)) {
                List<Tag> strings = mStringTags.get(campaignId);

                for (int i = 0; i < strings.size(); i++) {
                    tagStart += " " + strings.get(i).getName();
                    if (i != strings.size() - 1) {
                        tagStart += ",";
                    }
                }
            }

            tagStart += "</font>";
            String text = start + tagStart;
            textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
        }

        private String loadClient(int campaignId) {
            if (mClients == null || !mClients.containsKey(campaignId)) {
                return "";
            } else {
                return mClients.get(campaignId).getName();
            }
        }


        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(mLayout, viewGroup, false);
                viewHolder = new ViewHolder();

                viewHolder.mImage = (ImageView) convertView.findViewById(R.id.imgvCampaignImage);
                viewHolder.mClient = (TextView) convertView.findViewById(R.id.txtvCampaignClient);
                viewHolder.mTitle = (TextView) convertView.findViewById(R.id.txtvCampaignTitle);
                viewHolder.mDescription = (TextView) convertView.findViewById(R.id.txtvCampaignDescription);
                viewHolder.mStart = (TextView) convertView.findViewById(R.id.txtvCampaignStart);
                viewHolder.mEnd = (TextView) convertView.findViewById(R.id.txtvCampaignEnd);
                viewHolder.mTags = (TextView) convertView.findViewById(R.id.txtvCampaignTags);
                viewHolder.mProgress = (ProgressBar) convertView.findViewById(R.id.pbCampaignImage);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Campaign campaign = mData.get(position);
            if (campaign.getAvatar() != null) {
                try {
                    loadAvatar(campaign.getAvatar(), viewHolder.mImage, viewHolder.mProgress);
                } catch (IOException e) {
                    viewHolder.mImage.setImageResource(R.drawable.campaign_placeholder);
                }
            } else {
                viewHolder.mImage.setImageResource(R.drawable.campaign_placeholder);
            }

            viewHolder.mTitle.setText(campaign.getName());
            viewHolder.mDescription.setText(campaign.getDescription());
            viewHolder.mStart.setText(campaign.getStartDate());
            viewHolder.mEnd.setText(campaign.getEndDate());
            viewHolder.mClient.setText(loadClient(campaign.getId()));
            loadTags(viewHolder.mTags, campaign.getId());

            return convertView;
        }
    }

    @InjectView(R.id.lvProjects)
    private ListView mListView;

    @InjectView(R.id.pbProjects)
    private ProgressBar mProgressBar;

    @Inject
    private ImageUtils mImageUtils;

    private ArrayList<Campaign> mCampaigns = new ArrayList<>();
    private HashMap<Integer, Client> mClientsMap = new HashMap<>();
    private HashMap<Integer, List<Tag>> mTags = new HashMap<>();
    private HashMap<Integer, Person> mManagers = new HashMap<>();
    private CampaignListAdapter mListAdapter;
    private MainActivity mActivity;

    private void setAdapter() {
        mListAdapter = new CampaignListAdapter(MyApplication.getAppContext(),
                mCampaigns, R.layout.list_row_campaign);
        mListView.setAdapter(mListAdapter);
    }

    private void setListeners() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Campaign campaign = mCampaigns.get(position);
                mActivity.setFragment(CampaignTabFragment.getInstance(
                        campaign,
                        mClientsMap.get(campaign.getId()),
                        mTags.get(campaign.getId()),
                        mManagers.get(campaign.getId())), true);
            }
        });
    }

    private void loadCampaigns() {
        AsyncTask<Void, Void, Void> loaderTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                mListView.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                mCampaigns.clear();
                mTags.clear();
                mClientsMap.clear();
                mManagers.clear();
            }

            @Override
            protected Void doInBackground(Void... params) {
                CampaignDataSource ds = CampaignDataSource.getInstance();
                mCampaigns.addAll(ds.getAll());

                ClientDataSource cds = ClientDataSource.getInstance();
                PersonDataSource pds = PersonDataSource.getInstance();

                TagDataSource tct = TagDataSource.getInstance();

                for (Campaign campaign : mCampaigns) {
                    for (Client client : cds.getAll()) {
                        if (client.getId() == campaign.getClientId()) {
                            mClientsMap.put(campaign.getId(), client);
                            break;
                        }
                    }

                    for (Person person : pds.getAll()) {
                        if (person.getId() == campaign.getPersonId()) {
                            mManagers.put(campaign.getId(), person);
                            break;
                        }
                    }

                    List<Tag> tags = tct.getAllByCampaign(campaign.getId());

                    mTags.put(campaign.getId(), tags);
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mListAdapter.setClients(mClientsMap);
                mListAdapter.setTags(mTags);
                mListAdapter.notifyDataSetChanged();
                mProgressBar.setVisibility(View.GONE);
                mListView.setVisibility(View.VISIBLE);
            }
        };
        loaderTask.execute();
    }

    private void setToolbarTitle() {
        mActivity.setActionBarTitle("Projekty");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_projects, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
        setToolbarTitle();
        loadCampaigns();
        setListeners();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mActivity.getMenuInflater().inflate(R.menu.menu_new_task, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add) {
            mActivity.setFragment(new NewCampaignFragment(), true);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
