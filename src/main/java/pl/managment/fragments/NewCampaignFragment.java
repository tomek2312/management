package pl.managment.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.google.common.base.Optional;
import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import pl.managment.AvatarEvent;
import pl.managment.BundleConstants;
import pl.managment.ImageUtils;
import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.CampaignDataSource;
import pl.managment.db.ClientDataSource;
import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Client;
import pl.managment.db.enitity.Person;
import pl.managment.dialog.ClientDialogFragment;
import pl.managment.listeners.ClientListener;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class NewCampaignFragment extends RoboEventFragment implements DatePickerDialog.OnDateSetListener {
    public static final int REQUEST_CAMERA = 100;
    public static final int REQUEST_PICK_PICTURE = 101;
    public static final String DATEPICKER_TAG_1 = "datepicker1";
    public static final String DATEPICKER_TAG_2 = "datepicker2";
    private final static String TEMPORARY_SECOND = "temporary_second";

    @InjectView(R.id.edtCampaignName)
    private EditText mEdtName;

    @InjectView(R.id.edtCampaignDescription)
    private EditText mEdtDescription;

    @InjectView(R.id.edtCampaignEnd)
    private TextView mTxtvEnd;

    @InjectView(R.id.rlCampaignEnd)
    private RelativeLayout mLlEnd;

    @InjectView(R.id.edtCampaignStart)
    private TextView mTxtvStart;

    @InjectView(R.id.rlCampaignStart)
    private RelativeLayout mLlStart;

    @InjectView(R.id.txtvCampaignPerson)
    private TextView mTxtvPerson;

    @InjectView(R.id.rlCampaignPerson)
    private RelativeLayout mLlPerson;

    @InjectView(R.id.edtCampaignClient)
    private TextView mTxtvClient;

    @InjectView(R.id.imgvCampaignAvatar)
    private ImageView mImgvProfile;

    @InjectView(R.id.imgvClientAdd)
    private ImageView mImgvCleintAdd;

    @InjectView(R.id.imgvClientBase)
    private ImageView mImgvCleintBase;

    @InjectView(R.id.llCampaignTags)
    private LinearLayout mLlTags;

    @InjectView(R.id.edtCampaignTag)
    private EditText mEdtTagAdd;

    @InjectView(R.id.imgvCampaignTagAdd)
    private ImageView mImgvTagAdd;

    @Inject
    private ImageUtils mImageUtils;

    private MainActivity mActivity;
    private File mTemporaryFile;
    private String mAvatar = "";
    private DatePickerDialog mDialogStart;
    private DatePickerDialog mDialogEnd;
    private HashMap<Integer, Person> mPersonMap = new HashMap<>();
    private HashMap<Integer, Client> mClientMap = new HashMap<>();
    private ArrayList<String> mTags = new ArrayList<>();
    private Client mClient;
    private Person mPerson;

    private void setDrawer(final String avatar) {
        AsyncTask<Void, Void, Void> loader = new AsyncTask<Void, Void, Void>() {
            private Bitmap mAvatar;

            @Override
            protected void onPreExecute() {
                mAvatar = BitmapFactory.decodeResource(getResources(), R.drawable.campaign_placeholder);
            }

            @Override
            protected Void doInBackground(Void... params) {
                if (avatar != null && !TextUtils.isEmpty(avatar)) {
                    int size = (int) getResources().getDimension(R.dimen.dashboard_image_big);

                    File file = new File(avatar);
                    try {
                        Optional<Bitmap> optBitmap = mImageUtils.loadPreScaledBitmap(file,
                                size, size);
                        if (optBitmap.isPresent()) {
                            mAvatar = mImageUtils.transformToCircle(optBitmap.get());
                        } else {
                            mAvatar = mImageUtils.transformToCircle(mAvatar);
                        }
                    } catch (IOException e) {
                        mAvatar = mImageUtils.transformToCircle(mAvatar);
                    }
                } else {
                    mAvatar = mImageUtils.transformToCircle(mAvatar);
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mImgvProfile.setImageBitmap(mAvatar);
            }
        };

        loader.execute();
    }

    private void getPictureFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mTemporaryFile));
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void getPictureFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Wybierz zdjęcie"),
                REQUEST_PICK_PICTURE);
    }

    private void setFileForImage() {
        File cacheDir = MyApplication.getAppContext().getExternalCacheDir();
        mTemporaryFile = new File(cacheDir, TEMPORARY_SECOND);
//        mCurrentFile = new File(cacheDir, BundleConstants.FILE_USER_PROFILE_AVATAR);
    }

    private void loadPeople() {
        AsyncTask<Void, Void, String[]> loader = new AsyncTask<Void, Void, String[]>() {
            @Override
            protected void onPreExecute() {
                mPersonMap.clear();
            }

            @Override
            protected String[] doInBackground(Void... params) {
                PersonDataSource pds = PersonDataSource.getInstance();
                List<Person> people = pds.getAllManagers();
                String[] array = new String[people.size()];
                int i = 0;

                for (Person person : people) {
                    array[i] = person.getName() + " " + person.getSurname();
                    mPersonMap.put(i, person);
                    i++;
                }

                return array;
            }

            @Override
            protected void onPostExecute(String[] strings) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle("Zmień menadżera")
                        .setCancelable(true)
                        .setItems(strings, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mPerson = mPersonMap.get(which);
                                mTxtvPerson.setText(mPerson.getName() + " " + mPerson.getSurname());
                                dialog.dismiss();

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        };

        loader.execute();
    }

    private void addTag() {
        String tag = mEdtTagAdd.getText().toString();
        mTags.add(tag);
        mLlTags.addView(getLayout(tag));
        mEdtTagAdd.setText("");
    }

    private RelativeLayout getLayout(final String name) {
        final RelativeLayout layout = (RelativeLayout) LayoutInflater.from(mActivity).
                inflate(R.layout.view_tag, null);
        TextView txtvName = (TextView) layout.findViewById(R.id.txtvTagName);
        txtvName.setText(name);

        ImageView imgvRemove = (ImageView) layout.findViewById(R.id.imgvTagRemove);
        imgvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLlTags.removeView(layout);
                mTags.remove(name);
            }
        });

        return layout;
    }

    private void loadClients() {
        AsyncTask<Void, Void, String[]> loader = new AsyncTask<Void, Void, String[]>() {
            @Override
            protected void onPreExecute() {
                mClientMap.clear();
            }

            @Override
            protected String[] doInBackground(Void... params) {
                ClientDataSource pds = ClientDataSource.getInstance();
                List<Client> clients = pds.getAll();
                String[] array = new String[clients.size()];
                int i = 0;

                for (Client client : clients) {
                    array[i] = client.getName();
                    mClientMap.put(i, client);
                    i++;
                }

                return array;
            }

            @Override
            protected void onPostExecute(String[] strings) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle("Zmień klienta")
                        .setCancelable(true)
                        .setItems(strings, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mClient = mClientMap.get(which);
                                mTxtvClient.setText(mClient.getName());
                                dialog.dismiss();

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        };

        loader.execute();
    }

    private void createNewClient() {
        ClientDialogFragment fragment = ClientDialogFragment.getDialog(new ClientListener() {
            @Override
            public void onCreated(Client client) {
                mClient = client;
                mTxtvClient.setText(mClient.getName());
            }
        });

        fragment.show(mActivity.getSupportFragmentManager(), "dialog");
    }

    private void setListeners(Bundle savedInstanceState) {
        mImgvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle("Wybierz docelowe miejsce")
                        .setCancelable(true)
                        .setItems(R.array.photo_picker, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    getPictureFromGallery();
                                } else {
                                    getPictureFromCamera();
                                }

                                dialog.dismiss();

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        mImgvTagAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mEdtTagAdd.getText())) {
                    addTag();
                } else {
                    mActivity.showToast("Wpisz tag!");
                }
            }
        });

        mLlPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPeople();
            }
        });

        mImgvCleintBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadClients();
            }
        });

        mImgvCleintAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewClient();
            }
        });

        final Calendar calendar = Calendar.getInstance();
        mDialogStart = DatePickerDialog.newInstance(this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), true);

        mDialogEnd = DatePickerDialog.newInstance(this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), true);

        mLlStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogStart.setVibrate(true);
                mDialogStart.setYearRange(calendar.get(Calendar.YEAR), calendar.get(Calendar.YEAR) + 5);
                mDialogStart.setCloseOnSingleTapDay(false);
                mDialogStart.show(mActivity.getSupportFragmentManager(), DATEPICKER_TAG_1);
            }
        });

        mLlEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogEnd.setVibrate(true);
                mDialogEnd.setYearRange(calendar.get(Calendar.YEAR), calendar.get(Calendar.YEAR) + 5);
                mDialogEnd.setCloseOnSingleTapDay(false);
                mDialogEnd.show(mActivity.getSupportFragmentManager(), DATEPICKER_TAG_1);
            }
        });

        if (savedInstanceState != null) {
            DatePickerDialog dpd1 = (DatePickerDialog) mActivity.getSupportFragmentManager().findFragmentByTag(DATEPICKER_TAG_1);
            if (dpd1 != null) {
                dpd1.setOnDateSetListener(this);
            }

            DatePickerDialog dpd2 = (DatePickerDialog) mActivity.getSupportFragmentManager().findFragmentByTag(DATEPICKER_TAG_2);
            if (dpd2 != null) {
                dpd2.setOnDateSetListener(this);
            }
        }
    }

    private String getPathFromUri(Uri uri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = mActivity.getContentResolver().query(uri,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);
        cursor.close();

        return filePath;
    }

    private void saveCampaign() {
        AsyncTask<Void, Void, Void> saver = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Campaign campaign = new Campaign();
                campaign.setAvatar(mAvatar);
                campaign.setName(mEdtName.getText().toString());
                campaign.setDescription(mEdtDescription.getText().toString());
                campaign.setStartDate(mTxtvStart.getText().toString());
                campaign.setEndDate(mTxtvEnd.getText().toString());

                if (mClient != null) {
                    campaign.setClientId(mClient.getId());
                }

                if (mPerson != null) {
                    campaign.setPersonId(mPerson.getId());
                }

                CampaignDataSource cds = CampaignDataSource.getInstance();
                cds.insertWithTags(campaign, false, mTags);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mActivity.onBackPressed();
            }
        };

        saver.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_campaign, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFileForImage();
        setListeners(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Optional<Bitmap> optBitmap;
                mAvatar = mTemporaryFile.getAbsolutePath();

                try {
                    optBitmap = mImageUtils.loadPreScaledBitmap(mTemporaryFile,
                            BundleConstants.AVATAR_SIZE, BundleConstants.AVATAR_SIZE);
                    if (optBitmap.isPresent()) {
                        setDrawer(mAvatar);
                    }
                } catch (IOException e) {
                    Toast.makeText(MyApplication.getAppContext(), "Nie zaladowalo sie zdjecie!",
                            Toast.LENGTH_SHORT).show();
                }

            } else if (requestCode == REQUEST_PICK_PICTURE) {
                Uri fileUri = data.getData();

                if (fileUri != null) {
                    try {
                        mTemporaryFile = new File(getPathFromUri(fileUri));
                        mAvatar = mTemporaryFile.getAbsolutePath();
                        MyApplication.getEventBus().post(new AvatarEvent(mTemporaryFile.getAbsolutePath()));
                        Optional<Bitmap> optBitmap = mImageUtils.loadPreScaledBitmap(mTemporaryFile,
                                BundleConstants.AVATAR_SIZE, BundleConstants.AVATAR_SIZE);
                        if (optBitmap.isPresent()) {
                            setDrawer(mAvatar);
                        }
                    } catch (Exception e) {
                        Toast.makeText(MyApplication.getAppContext(), "Nie zaladowalo sie zdjecie!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mActivity.getMenuInflater().inflate(R.menu.menu_save_task, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            saveCampaign();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        if (datePickerDialog == mDialogStart) {
            mTxtvStart.setText(day + "-" + (month + 1) + "-" + year);
        } else {
            mTxtvEnd.setText(day + "-" + (month + 1) + "-" + year);
        }
    }
}
