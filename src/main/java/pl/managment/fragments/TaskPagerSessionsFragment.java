package pl.managment.fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import pl.managment.DateUtils;
import pl.managment.MainActivity;
import pl.managment.R;
import pl.managment.db.SessionsDataSource;
import pl.managment.db.enitity.Exercise;
import pl.managment.db.enitity.Session;
import roboguice.inject.InjectView;

/**
 *@author Tomasz Trybała
 */
public class TaskPagerSessionsFragment extends RoboEventFragment{
    @InjectView(R.id.llTaskSessions)
    private LinearLayout mLlSessions;

    private Exercise mExercise;
    private MainActivity mActivity;
    private ArrayList<Session> mSessions = new ArrayList<>();


    private void setData(Exercise exercise) {
        mExercise = exercise;
    }

    private void loadSessions() {
        for (Session session : mSessions) {
            mLlSessions.addView(getLayout(session.getStart(), session.getStop(),
                    session.getDescription(), session.getId()));
        }
    }

    private void deleteSession(final View layout, final int id) {
        AsyncTask<Void, Void, Void> deleter = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                SessionsDataSource sds = SessionsDataSource.getInstance();
                sds.delete(id);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mLlSessions.removeView(layout);
            }
        };

        deleter.execute();
    }

    private LinearLayout getLayout(String start, String stop, String description, final int id) {
        final LinearLayout layout = (LinearLayout) LayoutInflater.from(mActivity).
                inflate(R.layout.view_session_task, null);
        TextView txtvStart = (TextView) layout.findViewById(R.id.txtvSessionStart);
        txtvStart.setText(start);

        TextView txtvStop = (TextView) layout.findViewById(R.id.txtvSessionStop);
        txtvStop.setText(stop);

        TextView txtvDescription = (TextView) layout.findViewById(R.id.txtvSessionDescription);
        txtvDescription.setText(description);

        TextView txtvTime = (TextView) layout.findViewById(R.id.txtvSessionTime);
        txtvTime.setText(DateUtils.getSessionTime(start, stop));

        ImageView imgvDelete = (ImageView) layout.findViewById(R.id.imgvSessionDelete);
        imgvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSession(layout, id);
            }
        });

        return layout;
    }

    private void loadActivities() {
        AsyncTask<Void, Void, Void> loaderTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                mSessions.clear();
            }

            @Override
            protected Void doInBackground(Void... params) {
                SessionsDataSource sds = SessionsDataSource.getInstance();
                mSessions.addAll(sds.getAllByeExercise(mExercise.getId()));

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                loadSessions();
            }
        };

        loaderTask.execute();
    }

    public static TaskPagerSessionsFragment getInstance(Exercise exercise) {
        TaskPagerSessionsFragment fragment = new TaskPagerSessionsFragment();
        fragment.setData(exercise);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_sessions, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadActivities();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
