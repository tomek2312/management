package pl.managment.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.google.common.base.Optional;
import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import pl.managment.AvatarEvent;
import pl.managment.BundleConstants;
import pl.managment.ImageUtils;
import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Person;
import pl.managment.db.enums.PersonFunction;
import pl.managment.db.enums.PersonGender;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class AddPersonFragment extends RoboEventFragment implements DatePickerDialog.OnDateSetListener {
    public static final int REQUEST_CAMERA = 100;
    public static final int REQUEST_PICK_PICTURE = 101;
    public static final String DATEPICKER_TAG = "datepicker";
    private final static String TEMPORARY_SECOND = "temporary_second";

    @InjectView(R.id.edtProfileAddress)
    private EditText mEdtAddress;

    @InjectView(R.id.edtProfileName)
    private EditText mEdtName;

    @InjectView(R.id.edtProfileSurname)
    private EditText mEdtSurname;

    @InjectView(R.id.edtProfilePhone)
    private EditText mEdtPhone;

    @InjectView(R.id.edtProfileEmail)
    private EditText mEdtMail;

    @InjectView(R.id.edtProfilePassword)
    private EditText mEdtPassword;

    @InjectView(R.id.edtProfileDateOfBirth)
    private TextView mTxtvDateOfBirth;

    @InjectView(R.id.llProfileDateOfBirth)
    private LinearLayout mLlDateOfBirth;

    @InjectView(R.id.sPersonGender)
    private Spinner mSGender;

    @InjectView(R.id.sPersonFunction)
    private Spinner mSFunction;

    @InjectView(R.id.imgvProfile)
    private ImageView mImgvProfile;

    @Inject
    private ImageUtils mImageUtils;

    private MainActivity mActivity;
    private File mTemporaryFile;
    private String mAvatar = "";

    private void setBitmap(Bitmap bitmap) {
        try {
            Bitmap squareBitmap = mImageUtils.cropBitmapToSquare(bitmap);
            mImgvProfile.setImageBitmap(mImageUtils.transformToOval(
                    MyApplication.getAppContext(),
                    squareBitmap,
                    R.drawable.ic_profile_mask));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPictureFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mTemporaryFile));
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void getPictureFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Wybierz zdjęcie"),
                REQUEST_PICK_PICTURE);
    }

    private void setFileForImage() {
        File cacheDir = MyApplication.getAppContext().getExternalCacheDir();
        mTemporaryFile = new File(cacheDir, TEMPORARY_SECOND);
//        mCurrentFile = new File(cacheDir, BundleConstants.FILE_USER_PROFILE_AVATAR);
    }

    private void setListeners(Bundle savedInstanceState) {
        mImgvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle("Wybierz docelowe miejsce")
                        .setCancelable(true)
                        .setItems(R.array.photo_picker, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    getPictureFromGallery();
                                } else {
                                    getPictureFromCamera();
                                }

                                dialog.dismiss();

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), true);

        mLlDateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.setVibrate(true);
                datePickerDialog.setYearRange(1960, 2000);
                datePickerDialog.setCloseOnSingleTapDay(false);
                datePickerDialog.show(mActivity.getSupportFragmentManager(), DATEPICKER_TAG);
            }
        });

        if (savedInstanceState != null) {
            DatePickerDialog dpd = (DatePickerDialog) mActivity.getSupportFragmentManager().findFragmentByTag(DATEPICKER_TAG);
            if (dpd != null) {
                dpd.setOnDateSetListener(this);
            }
        }
    }

    private String getPathFromUri(Uri uri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = mActivity.getContentResolver().query(uri,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);
        cursor.close();

        return filePath;
    }

    private void savePerson() {
        AsyncTask<Void, Void, Void> saver = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Person person = new Person();
                person.setName(mEdtName.getText().toString());
                person.setSurname(mEdtSurname.getText().toString());
                person.setAddress(mEdtAddress.getText().toString());
                person.setDateBirth(mTxtvDateOfBirth.getText().toString());
                person.setFunction(getFunction());
                person.setGender(getGender());
                person.setMail(mEdtMail.getText().toString());
                person.setPassword(mEdtPassword.getText().toString());
                person.setPhone(mEdtPhone.getText().toString());
                person.setAvatar(mAvatar);

                PersonDataSource pds = PersonDataSource.getInstance();
                pds.insert(person, false);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mActivity.onBackPressed();
            }
        };

        saver.execute();
    }

    private String getGender() {
        int item = mSGender.getSelectedItemPosition();
        if (item == 0) {
            return PersonGender.MALE.getName();
        } else {
            return PersonGender.FEMALE.getName();
        }
    }

    private String getFunction() {
        int item = mSFunction.getSelectedItemPosition();
        if (item == 0) {
            return PersonFunction.ADMIN.getName();
        } else if (item == 1) {
            return PersonFunction.MANAGER.getName();
        } else {
            return PersonFunction.WORKER.getName();
        }
    }

    private void setAdapters() {
        ArrayAdapter<CharSequence> adapterGender = ArrayAdapter.createFromResource(mActivity,
                R.array.array_gender, android.R.layout.simple_spinner_item);
        adapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSGender.setAdapter(adapterGender);

        ArrayAdapter<CharSequence> adapterFunction = ArrayAdapter.createFromResource(mActivity,
                R.array.array_function, android.R.layout.simple_spinner_item);
        adapterFunction.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSFunction.setAdapter(adapterFunction);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_person, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapters();
        setFileForImage();
        setListeners(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Optional<Bitmap> optBitmap;
                mAvatar = mTemporaryFile.getAbsolutePath();

                try {
                    optBitmap = mImageUtils.loadPreScaledBitmap(mTemporaryFile,
                            BundleConstants.AVATAR_SIZE, BundleConstants.AVATAR_SIZE);
                    if (optBitmap.isPresent()) {
                        setBitmap(optBitmap.get());
                        MyApplication.getEventBus().post(new AvatarEvent(mTemporaryFile.getAbsolutePath()));
                    }
                } catch (IOException e) {
                    Toast.makeText(MyApplication.getAppContext(), "Nie zaladowalo sie zdjecie!",
                            Toast.LENGTH_SHORT).show();
                }

            } else if (requestCode == REQUEST_PICK_PICTURE) {
                Uri fileUri = data.getData();

                if (fileUri != null) {
                    try {
                        mTemporaryFile = new File(getPathFromUri(fileUri));
                        mAvatar = mTemporaryFile.getAbsolutePath();
                        MyApplication.getEventBus().post(new AvatarEvent(mTemporaryFile.getAbsolutePath()));
                        Optional<Bitmap> optBitmap = mImageUtils.loadPreScaledBitmap(mTemporaryFile,
                                BundleConstants.AVATAR_SIZE, BundleConstants.AVATAR_SIZE);
                        if (optBitmap.isPresent()) {
                            setBitmap(optBitmap.get());
                        }
                    } catch (Exception e) {
                        Toast.makeText(MyApplication.getAppContext(), "Nie zaladowalo sie zdjecie!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mActivity.getMenuInflater().inflate(R.menu.menu_save_task, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            savePerson();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        mTxtvDateOfBirth.setText(day + "-" + month + "-" + year);
    }
}
