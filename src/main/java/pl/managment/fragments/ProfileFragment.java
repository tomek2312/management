package pl.managment.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Optional;
import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import pl.managment.AvatarEvent;
import pl.managment.BundleConstants;
import pl.managment.ImageUtils;
import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Person;
import pl.managment.db.enums.PersonFunction;
import pl.managment.db.enums.PersonGender;
import pl.managment.dialog.ProfileChangeDialog;
import pl.managment.listeners.ProfileListener;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class ProfileFragment extends RoboEventFragment {
    public static final int REQUEST_CAMERA = 100;
    public static final int REQUEST_PICK_PICTURE = 101;
    private final static String TEMPORARY_SECOND = "temporary_second";

    @InjectView(R.id.llContainer)
    private LinearLayout mLlContainer;

    @InjectView(R.id.llProfilePassword)
    private LinearLayout mLlPassword;

    @InjectView(R.id.progressBar)
    private ProgressBar mProgressBar;

    @InjectView(R.id.txtvProfileError)
    private TextView mTxtvError;

    @InjectView(R.id.imgvProfile)
    private ImageView mImgvProfile;

    @InjectView(R.id.txtvProfileName)
    private TextView mTxtvProfileName;

    @InjectView(R.id.txtvProfileJob)
    private TextView mTxtvProfileJob;

    @InjectView(R.id.edtProfileDateOfBirth)
    private EditText mEdtDateOfBirth;

    @InjectView(R.id.edtProfileAddress)
    private EditText mEdtAddress;

    @InjectView(R.id.edtProfilePhone)
    private EditText mEdtPhone;

    @InjectView(R.id.edtProfileEmail)
    private EditText mEdtEmail;

    @InjectView(R.id.edtProfilePassword)
    private EditText mEdtPassword;

    @InjectView(R.id.datePicker)
    private DatePicker mDatePicker;

    @InjectView(R.id.sProfileFunction)
    private Spinner mSFunction;

    @Inject
    private ImageUtils mImageUtils;

    private int mPersonId;
    private MainActivity mActivity;
    private Person mPerson;
    private File mTemporaryFile;
    private int mCurrentId;
    private boolean mIsCurrentAdmin;
    private boolean mIsEdit;

    private void setId(int personId) {
        mPersonId = personId;
    }

    public static ProfileFragment getInstance(int personId) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.setId(personId);

        return fragment;
    }

    private String getFunction() {
        int item = mSFunction.getSelectedItemPosition();
        if (item == 0) {
            return PersonFunction.ADMIN.getName();
        } else if (item == 1) {
            return PersonFunction.MANAGER.getName();
        } else {
            return PersonFunction.WORKER.getName();
        }
    }

    private void setAdapters() {
        ArrayAdapter<CharSequence> adapterFunction = ArrayAdapter.createFromResource(mActivity,
                R.array.array_function, android.R.layout.simple_spinner_item);
        adapterFunction.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSFunction.setAdapter(adapterFunction);

        if (mTxtvProfileJob.getText().toString().equals(PersonFunction.ADMIN.getName())) {
            mSFunction.setSelection(0);
        } else if (mTxtvProfileJob.getText().toString().equals(PersonFunction.MANAGER.getName())) {
            mSFunction.setSelection(1);
        } else {
            mSFunction.setSelection(2);
        }
    }

    private void loadData() {
        AsyncTask<Void, Void, Person> loader = new AsyncTask<Void, Void, Person>() {
            @Override
            protected Person doInBackground(Void... params) {
                Person p = null;

                mCurrentId = Integer.parseInt(
                        MyApplication.loadFromSettings(BundleConstants.LOGIN_PERSON_ID).get());
                PersonDataSource dataSource = PersonDataSource.getInstance();
                List<Person> people = dataSource.getAll();
                for (Person person : people) {
                    if (mCurrentId != mPersonId && person.getId() == mCurrentId) {
                        if (person.getFunction() == PersonFunction.ADMIN) {
                            mIsCurrentAdmin = true;
                        }
                    }

                    if (p == null && person.getId() == mPersonId) {
                        p = person;
                    }
                }

                return p;
            }

            @Override
            protected void onPostExecute(Person person) {
                if (person != null) {
                    mPerson = person;
                    displayPerson();
                    setAdapters();
                    mActivity.invalidateOptionsMenu();
                    try {
                        loadAvatar();
                    } catch (IOException e) {
                        mImgvProfile.setImageResource(mPerson.getGender() == PersonGender.MALE ?
                                R.drawable.male_avatar :
                                R.drawable.female_avatar);
                        mProgressBar.setVisibility(View.GONE);
                        mLlContainer.setVisibility(View.VISIBLE);
                    }
                } else {
                    mProgressBar.setVisibility(View.GONE);
                    mTxtvError.setVisibility(View.VISIBLE);
                }
            }
        };

        loader.execute();
    }

    private void loadAvatar() throws IOException {
        if (mPerson.getAvatar() != null && !TextUtils.isEmpty(mPerson.getAvatar())) {
            File tempImage = new File(mPerson.getAvatar());
            if (tempImage.exists()) {
                Optional<Bitmap> bitmapOptional = mImageUtils.loadPreScaledBitmap(tempImage,
                        BundleConstants.AVATAR_SIZE, BundleConstants.AVATAR_SIZE);
                if (bitmapOptional.isPresent()) {
                    setBitmap(bitmapOptional.get());
                }
            }
        }

        mProgressBar.setVisibility(View.GONE);
        mLlContainer.setVisibility(View.VISIBLE);
    }

    private void displayEditDialog() {
        ProfileChangeDialog dialog = ProfileChangeDialog.getDialog(
                mPerson, mTxtvProfileJob.getText().toString(),
                mPerson.getFunction() == PersonFunction.ADMIN || mIsCurrentAdmin,
                new ProfileListener() {
                    @Override
                    public void onChange(String function, String dateOfBirth, String address, String phone, String password) {
                        if(function != null){
                            mTxtvProfileJob.setText(function);
                        }

                        mPerson.setPhone(phone);
                        mPerson.setAddress(address);
                        mPerson.setPassword(password);
                        mPerson.setDateBirth(dateOfBirth);
                        displayPerson();
                    }
                });
        dialog.show(mActivity.getSupportFragmentManager(), "dialog");
    }

    private void setBitmap(Bitmap bitmap) {
        try {
            Bitmap squareBitmap = mImageUtils.cropBitmapToSquare(bitmap);
            mImgvProfile.setImageBitmap(mImageUtils.transformToOval(
                    MyApplication.getAppContext(),
                    squareBitmap,
                    R.drawable.ic_profile_mask));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPictureFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mTemporaryFile));
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void getPictureFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Wybierz zdjęcie"),
                REQUEST_PICK_PICTURE);
    }

    private void setFileForImage() {
        File cacheDir = MyApplication.getAppContext().getExternalCacheDir();
        mTemporaryFile = new File(cacheDir, TEMPORARY_SECOND);
//        mCurrentFile = new File(cacheDir, BundleConstants.FILE_USER_PROFILE_AVATAR);
    }

    private void displayPerson() {
        mTxtvProfileName.setText(mPerson.getName() + " " + mPerson.getSurname());
        mTxtvProfileJob.setText(mPerson.getFunction().getName());
        mEdtDateOfBirth.setText(mPerson.getDateBirth());
        mEdtPhone.setText(mPerson.getPhone());
        mEdtEmail.setText(mPerson.getMail());
        mEdtAddress.setText(mPerson.getAddress());
    }

    private void editProfile() {
        if (!mIsEdit) {
            mEdtAddress.setEnabled(true);
            mEdtAddress.setFocusable(true);
            mEdtAddress.setFocusableInTouchMode(true);
            mEdtPhone.setEnabled(true);
            mEdtPhone.setFocusable(true);
            mEdtPhone.setFocusableInTouchMode(true);
            mLlPassword.setVisibility(View.VISIBLE);
            mEdtPassword.setText(mPerson.getPassword());

            if (mPerson.getFunction() == PersonFunction.ADMIN || mIsCurrentAdmin) {
                mTxtvProfileJob.setVisibility(View.GONE);
                mSFunction.setVisibility(View.VISIBLE);
            }

            mImgvProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setTitle("Wybierz docelowe miejsce")
                            .setCancelable(true)
                            .setItems(R.array.photo_picker, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == 0) {
                                        getPictureFromGallery();
                                    } else {
                                        getPictureFromCamera();
                                    }

                                    dialog.dismiss();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

            String[] date = mEdtDateOfBirth.getText().toString().split("-");
            mDatePicker.init(Integer.parseInt(date[2]), Integer.parseInt(date[1]), Integer.parseInt(date[0]), null);
            mDatePicker.setVisibility(View.VISIBLE);
            mEdtDateOfBirth.setVisibility(View.GONE);
        } else {
            mPerson.setPhone(mEdtPhone.getText().toString());
            mPerson.setAddress(mEdtAddress.getText().toString());
            mPerson.setPassword(mEdtPassword.getText().toString());
            mPerson.setDateBirth(mDatePicker.getDayOfMonth() + "-" +
                    mDatePicker.getMonth() + "-" +
                    mDatePicker.getYear());

            if (mPerson.getFunction() == PersonFunction.ADMIN || mIsCurrentAdmin) {
                mTxtvProfileJob.setText(getFunction());
                mTxtvProfileJob.setVisibility(View.VISIBLE);
                mSFunction.setVisibility(View.GONE);
            }

            mEdtAddress.setEnabled(false);
            mEdtAddress.setFocusable(false);
            mEdtAddress.setFocusableInTouchMode(false);
            mEdtPhone.setEnabled(false);
            mEdtPhone.setFocusable(false);
            mEdtPhone.setFocusableInTouchMode(false);
            mLlPassword.setVisibility(View.GONE);

            mImgvProfile.setOnClickListener(null);
            mDatePicker.setVisibility(View.GONE);
            mEdtDateOfBirth.setText(mPerson.getDateBirth());
            mEdtDateOfBirth.setVisibility(View.VISIBLE);

        }

        mIsEdit = !mIsEdit;
    }

    private String getPathFromUri(Uri uri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = mActivity.getContentResolver().query(uri,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);
        cursor.close();

//        String[] projection = {MediaStore.Images.Media.DATA};
//        CursorLoader loader = new CursorLoader(MyApplication.getAppContext(), uri, projection, null, null, null);
//        Cursor cursor = loader.loadInBackground();
//
//        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//        cursor.moveToFirst();
//        String filePath = cursor.getString(column_index);
//        cursor.close();

        return filePath;
    }

    private void saveProfile() {
        AsyncTask<Void, Void, Void> saver = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
//                mPerson.setPhone(mEdtPhone.getText().toString());
//                mPerson.setAddress(mEdtAddress.getText().toString());
//                mPerson.setPassword(mEdtPassword.getText().toString());
//                mPerson.setDateBirth(mDatePicker.getDayOfMonth() + "-" +
//                        mDatePicker.getMonth() + "-" +
//                        mDatePicker.getYear());
                mPerson.setFunction(getFunction());
                PersonDataSource dataSource = PersonDataSource.getInstance();
                dataSource.insert(mPerson, true);

                return null;
            }
        };

        if (mPerson != null) {
            saver.execute();
        }
    }

    private void delete() {
        AsyncTask<Void, Void, Void> deleter = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                PersonDataSource cds = PersonDataSource.getInstance();
                cds.delete(mPerson.getId());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mPerson = null;
                mActivity.onBackPressed();
            }
        };

        deleter.execute();
    }

    private void setToolbarTitle() {
        mActivity.setActionBarTitle("Profil");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbarTitle();
        setFileForImage();
        loadData();
    }

    @Override
    public void onDestroy() {
        saveProfile();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Optional<Bitmap> optBitmap;
                mPerson.setAvatar(mTemporaryFile.getAbsolutePath());

                try {
                    optBitmap = mImageUtils.loadPreScaledBitmap(mTemporaryFile,
                            BundleConstants.AVATAR_SIZE, BundleConstants.AVATAR_SIZE);
                    if (optBitmap.isPresent()) {
                        setBitmap(optBitmap.get());
                        MyApplication.getEventBus().post(new AvatarEvent(mTemporaryFile.getAbsolutePath()));
                    }
                } catch (IOException e) {
                    Toast.makeText(MyApplication.getAppContext(), "Nie zaladowalo sie zdjecie!",
                            Toast.LENGTH_SHORT).show();
                }

            } else if (requestCode == REQUEST_PICK_PICTURE) {
                Uri fileUri = data.getData();

                if (fileUri != null) {
                    try {
                        mTemporaryFile = new File(getPathFromUri(fileUri));
                        mPerson.setAvatar(mTemporaryFile.getAbsolutePath());
                        MyApplication.getEventBus().post(new AvatarEvent(mTemporaryFile.getAbsolutePath()));
                        Optional<Bitmap> optBitmap = mImageUtils.loadPreScaledBitmap(mTemporaryFile,
                                BundleConstants.AVATAR_SIZE, BundleConstants.AVATAR_SIZE);
                        if (optBitmap.isPresent()) {
                            setBitmap(optBitmap.get());
                        }
                    } catch (Exception e) {
                        Toast.makeText(MyApplication.getAppContext(), "Nie zaladowalo sie zdjecie!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem editItem = menu.findItem(R.id.action_edit);
        MenuItem deleteItem = menu.findItem(R.id.action_delete);
        if (mCurrentId == mPersonId || mIsCurrentAdmin) {
            editItem.setVisible(true);
            deleteItem.setVisible(true);
        } else {
            editItem.setVisible(false);
            deleteItem.setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mActivity.getMenuInflater().inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_delete) {
            delete();

            return true;
        }

        if (id == R.id.action_edit) {
            //todo uncomment
//            editProfile();
            displayEditDialog();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
