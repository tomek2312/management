package pl.managment.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.managment.ImageUtils;
import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.ExerciseDataSource;
import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Exercise;
import pl.managment.db.enitity.Person;
import pl.managment.view.FillImageView;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class CampaignTasksFragment extends RoboEventFragment {
    /*package*/ static class TasksListAdapter extends ArrayAdapter<Exercise> {
        private final Context mContext;

        private final ArrayList<Exercise> mData;
        private final int mLayout;
        protected HashMap<Integer, Campaign> mCampaigns;
        private HashMap<Integer, Person> mWorkers;

        private class ViewHolder {
            public ImageView mPersonAvatar;
            public TextView mCode;
            public TextView mDescription;
            public TextView mPerson;
            public TextView mStatus;
            public FillImageView mProgress;
        }

        public TasksListAdapter(Context context, ArrayList<Exercise> data, int layout) {
            super(context, layout, data);

            mContext = context;
            mData = data;
            mLayout = layout;
        }

        private void loadPerson(ImageView imageView, TextView textView, int exerciseId) {
            if (mWorkers.containsKey(exerciseId)) {
                Person person = mWorkers.get(exerciseId);
                textView.setText(person.getName().substring(0, 1) + ". " + person.getSurname());
                try {
                    loadAvatar(person.getAvatar(), imageView);
                } catch (IOException e) {
                    imageView.setImageResource(R.drawable.male_avatar);
                }

            } else {
                textView.setText("---");
                imageView.setImageResource(R.drawable.male_avatar);
            }
        }

        private void loadAvatar(final String avatar, final ImageView imageView)
                throws IOException {
            AsyncTask<Void, Void, Void> loader = new AsyncTask<Void, Void, Void>() {
                private Bitmap mBitmap = BitmapFactory.decodeResource(
                        MyApplication.getAppContext().getResources(),
                        R.drawable.male_avatar);

                @Override
                protected void onPreExecute() {
                    imageView.setVisibility(View.INVISIBLE);
                }

                @Override
                protected Void doInBackground(Void... params) {
                    if (avatar != null && !TextUtils.isEmpty(avatar)) {
                        File tempImage = new File(avatar);
                        if (tempImage.exists()) {
                            int size = (int) MyApplication.getAppContext().getResources().
                                    getDimension(R.dimen.dashboard_image_small);
                            ImageUtils utils = new ImageUtils();
                            //todo change *2
                            mBitmap = utils.getResizedBitmap(size * 4, size * 2, tempImage.getAbsolutePath());
//                            mBitmap = utils.getRoundedTaskBitmap(bitmap);
                        }
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    imageView.setImageBitmap(mBitmap);
                    imageView.setVisibility(View.VISIBLE);
                }
            };

            loader.execute();
        }

        public void setData(HashMap<Integer, Campaign> campaigns, HashMap<Integer, Person> workers) {
            mWorkers = workers;
            mCampaigns = campaigns;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(mLayout, viewGroup, false);
                viewHolder = new ViewHolder();

                viewHolder.mPersonAvatar = (ImageView) convertView.findViewById(R.id.imgvCampaignImage);
                viewHolder.mCode = (TextView) convertView.findViewById(R.id.txtvCampaignCode);
                viewHolder.mProgress = (FillImageView) convertView.findViewById(R.id.fivCampaignProgress);
                viewHolder.mDescription = (TextView) convertView.findViewById(R.id.txtvCampaignDescription);
                viewHolder.mPerson = (TextView) convertView.findViewById(R.id.txtvCampaignPerson);
                viewHolder.mStatus = (TextView) convertView.findViewById(R.id.txtvCampaignStatus);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Exercise exercise = mData.get(position);
            viewHolder.mProgress.animateView(exercise.getProgress());
            viewHolder.mDescription.setText(exercise.getDescription());
            viewHolder.mCode.setText(mCampaigns.get(exercise.getId()).getName().substring(0, 3).toUpperCase() + "-" + exercise.getId());
            viewHolder.mStatus.setText(exercise.getStatus().getDisplayName().toUpperCase());
            loadPerson(viewHolder.mPersonAvatar, viewHolder.mPerson, exercise.getId());

            return convertView;
        }
    }

//    @InjectView(R.id.lvProjects)
//    private ListView mListView;

    @InjectView(R.id.gvProjects)
    private GridView mGridView;

    @InjectView(R.id.pbProjects)
    private ProgressBar mProgressBar;

    @Inject
    private ImageUtils mImageUtils;

    private MainActivity mActivity;
    private TasksListAdapter mListAdapter;
    protected ArrayList<Exercise> mTasks = new ArrayList<>();
    protected HashMap<Integer, Person> mPeople = new HashMap<>();
    protected HashMap<Integer, Campaign> mCampaigns = new HashMap<>();
    protected Campaign mCampaign;

    private void setData(Campaign campaign) {
        mCampaign = campaign;
    }

    private void setAdapter() {
        mListAdapter = new TasksListAdapter(MyApplication.getAppContext(),
                mTasks, R.layout.list_row_task_grid);
        mGridView.setAdapter(mListAdapter);
    }

    private void setListeners() {
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Exercise exercise = mTasks.get(position);
                mActivity.setFragment(TaskPagerFragment.getInstance(exercise,
                        mPeople.get(exercise.getId()),
                        mCampaigns.get(exercise.getId())), true);
            }
        });
    }

    protected void load() {
        ExerciseDataSource eds = ExerciseDataSource.getInstance();
        mTasks.addAll(eds.getAllByCampaign(mCampaign.getId()));

        PersonDataSource pds = PersonDataSource.getInstance();
        List<Person> people = pds.getAll();

        for (Exercise exercise : mTasks) {
            for (Person person : people) {
                if (exercise.getPersonId() == person.getId()) {
                    mPeople.put(exercise.getId(), person);
                    break;
                }
            }
            mCampaigns.put(exercise.getId(), mCampaign);
        }
    }

    private void loadTasks() {
        AsyncTask<Void, Void, Void> loaderTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                mGridView.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                mTasks.clear();
                mPeople.clear();
            }

            @Override
            protected Void doInBackground(Void... params) {
                load();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mListAdapter.setData(mCampaigns, mPeople);
                mListAdapter.notifyDataSetChanged();
                mProgressBar.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
            }
        };
        loaderTask.execute();
    }

    private void setToolbarTitle() {
        mActivity.setActionBarTitle("Taski");
    }

    public static CampaignTasksFragment getInstance(Campaign campaign) {
        CampaignTasksFragment fragment = new CampaignTasksFragment();
        fragment.setData(campaign);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_projects, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbarTitle();
        setAdapter();
        loadTasks();
        setListeners();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mCampaign != null) {
            mActivity.getMenuInflater().inflate(R.menu.menu_new_task, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add) {
            mActivity.setFragment(NewTaskFragment.createInstance(mCampaign), true);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
