package pl.managment.fragments;

import android.os.Bundle;

import com.google.common.eventbus.EventBus;

import pl.managment.MyApplication;
import roboguice.fragment.RoboFragment;

/**
 * Class of Fragment which implements RoboContext
 * that allows user to use RoboGuice resources
 *
 * @author Tomasz Trybała
 */
public class RoboEventFragment extends RoboFragment {
    protected EventBus mEventBus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEventBus = MyApplication.getEventBus();
    }

    @Override
    public void onStop() {
        super.onStop();
        mEventBus.unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mEventBus.register(this);
    }
}
