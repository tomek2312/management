package pl.managment.fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;

import java.util.Calendar;

import pl.managment.DateUtils;
import pl.managment.MainActivity;
import pl.managment.R;
import pl.managment.db.SessionsDataSource;
import pl.managment.db.enitity.Session;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class NewSessionFragment extends RoboEventFragment implements
        DatePickerDialog.OnDateSetListener {
    public static final String DATE_PICKER_TAG_1 = "datepicker1";
    public static final String DATE_PICKER_TAG_2 = "datepicker2";
    public static final String HOUR_PICKER_TAG_1 = "hourpicker1";
    public static final String HOUR_PICKER_TAG_2 = "hourpicker2";

    @InjectView(R.id.edtSessionDescription)
    private EditText mEdtDescription;

    @InjectView(R.id.llSessionStartDay)
    private RelativeLayout mLlStartDay;

    @InjectView(R.id.llSessionStartHour)
    private RelativeLayout mLlStartHour;

    @InjectView(R.id.llSessionEndDay)
    private RelativeLayout mLlEndDay;

    @InjectView(R.id.llSessionEndHour)
    private RelativeLayout mLlEndHour;

    @InjectView(R.id.edtCampaignStartDay)
    private TextView mTxtvStartDay;

    @InjectView(R.id.edtCampaignStartHour)
    private TextView mTxtvStartHour;

    @InjectView(R.id.edtCampaignEndDay)
    private TextView mTxtvEndDay;

    @InjectView(R.id.edtCampaignEndHour)
    private TextView mTxtvEndHour;

    private int mExerciseId;
    private DatePickerDialog mDateStart;
    private DatePickerDialog mDateEnd;
    private TimePickerDialog mTimeStart;
    private TimePickerDialog mTimeEnd;
    private MainActivity mActivity;

    private void setData(int exerciseId) {
        mExerciseId = exerciseId;
    }

    private void setListeners() {
        final Calendar calendar = Calendar.getInstance();

        mLlStartDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDateStart.setVibrate(true);
                mDateStart.setYearRange(calendar.get(Calendar.YEAR) - 1, calendar.get(Calendar.YEAR) + 1);
                mDateStart.setCloseOnSingleTapDay(false);
                mDateStart.show(mActivity.getSupportFragmentManager(), DATE_PICKER_TAG_1);
            }
        });

        mLlEndDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDateEnd.setVibrate(true);
                mDateEnd.setYearRange(calendar.get(Calendar.YEAR) - 1, calendar.get(Calendar.YEAR) + 1);
                mDateEnd.setCloseOnSingleTapDay(false);
                mDateEnd.show(mActivity.getSupportFragmentManager(), DATE_PICKER_TAG_2);
            }
        });

        mLlStartHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTimeStart.setVibrate(true);
                mTimeStart.setCloseOnSingleTapMinute(false);
                mTimeStart.show(mActivity.getSupportFragmentManager(), HOUR_PICKER_TAG_1);
            }
        });

        mLlEndHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTimeEnd.setVibrate(true);
                mTimeEnd.setCloseOnSingleTapMinute(false);
                mTimeEnd.show(mActivity.getSupportFragmentManager(), HOUR_PICKER_TAG_2);
            }
        });
    }

    private void setPickers(Bundle savedInstanceState) {
        TimePickerDialog.OnTimeSetListener mStartListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout radialPickerLayout, int i, int i2) {
                mTxtvStartHour.setText(getDoubleNumber(i) + ":" + getDoubleNumber(i2));
            }
        };

        TimePickerDialog.OnTimeSetListener mEndListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout radialPickerLayout, int i, int i2) {
                mTxtvEndHour.setText(getDoubleNumber(i) + ":" + getDoubleNumber(i2));
            }
        };

        final Calendar calendar = Calendar.getInstance();

        mDateStart = DatePickerDialog.newInstance(this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), true);

        mDateEnd = DatePickerDialog.newInstance(this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), true);

        mTimeStart = TimePickerDialog.newInstance(mStartListener,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                false, false);

        mTimeEnd = TimePickerDialog.newInstance(mEndListener,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                false, false);

        if (savedInstanceState != null) {
            DatePickerDialog dpd1 = (DatePickerDialog) mActivity.getSupportFragmentManager().findFragmentByTag(DATE_PICKER_TAG_1);
            if (dpd1 != null) {
                dpd1.setOnDateSetListener(this);
            }

            DatePickerDialog dpd2 = (DatePickerDialog) mActivity.getSupportFragmentManager().findFragmentByTag(DATE_PICKER_TAG_2);
            if (dpd2 != null) {
                dpd2.setOnDateSetListener(this);
            }

            TimePickerDialog tpd1 = (TimePickerDialog) mActivity.getSupportFragmentManager().findFragmentByTag(HOUR_PICKER_TAG_1);
            if (tpd1 != null) {
                tpd1.setOnTimeSetListener(mStartListener);
            }

            TimePickerDialog tpd2 = (TimePickerDialog) mActivity.getSupportFragmentManager().findFragmentByTag(HOUR_PICKER_TAG_2);
            if (tpd2 != null) {
                tpd2.setOnTimeSetListener(mEndListener);
            }
        }
    }

    private String getDoubleNumber(int i) {
        if (i < 10) {
            return "0" + i;
        } else {
            return "" + i;
        }
    }

    private void saveSession() {
        AsyncTask<Void, Void, Void> saver = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Session session = new Session();
                session.setDescription(mEdtDescription.getText().toString());
                session.setExerciseId(mExerciseId);
                session.setStart(mTxtvStartDay.getText().toString() + " " + mTxtvStartHour.getText().toString());
                session.setStop(mTxtvEndDay.getText().toString() + " " + mTxtvEndHour.getText().toString());

                SessionsDataSource sds = SessionsDataSource.getInstance();
                sds.insert(session);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mActivity.onBackPressed();
            }
        };

        saver.execute();
    }

    private void validate() {
        if (!TextUtils.isEmpty(mEdtDescription.getText()) &&
                !TextUtils.isEmpty(mTxtvStartHour.getText()) &&
                !TextUtils.isEmpty(mTxtvEndHour.getText())) {
            if (validateDate()) {
                saveSession();
            } else {
                mActivity.showToast("Data zakończenia nie może być wcześniejsza od daty rozpoczęcia!");
            }
        } else {
            mActivity.showToast("Wypełnij wszytskie dane!");
        }
    }

    private int getInt(String s) {
        return Integer.parseInt(s);
    }

    private boolean validateDate() {
        String[] dateStart = mTxtvStartDay.getText().toString().split("-");
        String[] dateEnd = mTxtvEndDay.getText().toString().split("-");
        String[] hourStart = mTxtvStartHour.getText().toString().split(":");
        String[] hourEnd = mTxtvEndHour.getText().toString().split(":");

        return getInt(dateStart[2]) <= getInt(dateEnd[2]) &&
                getInt(dateStart[1]) <= getInt(dateEnd[1]) &&
                getInt(dateStart[0]) <= getInt(dateEnd[0]) &&
                getInt(hourStart[0]) <= getInt(hourEnd[0]) &&
                getInt(hourStart[1]) <= getInt(hourEnd[1]);
    }

    private void setDefaultValues() {
        mTxtvStartDay.setText(DateUtils.getCurrentSessionDate());
        mTxtvEndDay.setText(DateUtils.getCurrentSessionDate());
    }

    public static NewSessionFragment getInstance(int exerciseId) {
        NewSessionFragment fragment = new NewSessionFragment();
        fragment.setData(exerciseId);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_session, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setPickers(savedInstanceState);
        setDefaultValues();
        setListeners();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mActivity.getMenuInflater().inflate(R.menu.menu_save_task, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            validate();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        if (datePickerDialog == mDateStart) {
            mTxtvStartDay.setText(getDoubleNumber(day) + "-" + getDoubleNumber(month + 1) + "-" + year);
        } else {
            mTxtvEndDay.setText(getDoubleNumber(day) + "-" + getDoubleNumber(month + 1) + "-" + year);
        }
    }
}
