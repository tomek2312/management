package pl.managment.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.base.Optional;

import java.util.HashMap;
import java.util.List;

import pl.managment.BundleConstants;
import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.ExerciseDataSource;
import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Exercise;
import pl.managment.db.enitity.Person;
import pl.managment.db.enums.ExercisePriority;
import pl.managment.db.enums.ExerciseStatus;
import pl.managment.dialog.ProgressDialogFragment;
import pl.managment.listeners.ProgressListener;
import roboguice.inject.InjectView;

/**
 *@author Tomasz Trybała
 */
public class TaskPagerDetailsFragment extends RoboEventFragment {
    @InjectView(R.id.txtvTaskPriority)
    private TextView mTxtvPriority;

    @InjectView(R.id.txtvTaskStatus)
    private TextView mTxtvStatus;

    @InjectView(R.id.txtvTaskPerson)
    private TextView mTxtvPerson;

    @InjectView(R.id.txtvTaskProgress)
    private TextView mTxtvProgress;

    @InjectView(R.id.txtvTaskTime)
    private TextView mTxtvTime;

    @InjectView(R.id.txtvTaskCreated)
    private TextView mTxtvCreated;

    @InjectView(R.id.txtvTaskCampaign)
    private TextView mTxtvCampaign;

    private MainActivity mActivity;
    private Exercise mExercise;
    private Person mPerson;
    private Campaign mCampaign;
    private HashMap<Integer, Person> mPersonMap = new HashMap<>();

    private void setData(Exercise exercise, Person person, Campaign campaign) {
        mExercise = exercise;
        mPerson = person;
        mCampaign = campaign;
    }

    public static TaskPagerDetailsFragment getInstance(Exercise exercise, Person person, Campaign campaign) {
        TaskPagerDetailsFragment fragment = new TaskPagerDetailsFragment();
        fragment.setData(exercise, person, campaign);

        return fragment;
    }

    private boolean isChangePossibility() {
        Optional<String> profile = MyApplication.loadFromSettings(BundleConstants.LOGIN_PERSON_ID);
        return profile.isPresent() && mPerson != null && mPerson.getId() == Integer.parseInt(profile.get());
    }

    private void displayData() {
        if (isChangePossibility()) {
            mTxtvPriority.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_edit_72_blue, 0);
            mTxtvPriority.setBackgroundResource(R.drawable.text_bg);
            mTxtvStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_edit_72_blue, 0);
            mTxtvStatus.setBackgroundResource(R.drawable.text_bg);
            mTxtvProgress.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_edit_72_blue, 0);
            mTxtvProgress.setBackgroundResource(R.drawable.text_bg);
            mTxtvPerson.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_edit_72_blue, 0);
            mTxtvPerson.setBackgroundResource(R.drawable.text_bg);

            mTxtvStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setTitle("Zmień status")
                            .setCancelable(true)
                            .setItems(R.array.exercise_status, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == 0) {
                                        mExercise.setStatus(ExerciseStatus.CREATE.getName());
                                    } else if (which == 1) {
                                        mExercise.setStatus(ExerciseStatus.IN_PROGRESS.getName());
                                    } else {
                                        mExercise.setStatus(ExerciseStatus.CLOSED.getName());
                                    }

                                    saveTask();
                                    displayData();
                                    dialog.dismiss();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

            mTxtvPriority.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setTitle("Zmień priorytet")
                            .setCancelable(true)
                            .setItems(R.array.exercise_priority, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == 0) {
                                        mExercise.setPriority(ExercisePriority.LOW.getName());
                                    } else if (which == 1) {
                                        mExercise.setPriority(ExercisePriority.NORMAL.getName());
                                    } else {
                                        mExercise.setPriority(ExercisePriority.HIGH.getName());
                                    }

                                    saveTask();
                                    displayData();
                                    dialog.dismiss();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

            mTxtvProgress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgressDialog();
                }
            });

            mTxtvPerson.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadPeople();
                }
            });
        }

        if (mExercise != null) {
            String priority = String.format(
                    "<font color='#909191'>Priorytet: </font><font color='#424242'>%s</font>",
                    mExercise.getPriority().getDisplayName());
            mTxtvPriority.setText(Html.fromHtml(priority));

            String status = String.format(
                    "<font color='#909191'>Status: </font><font color='#424242'>%s</font>",
                    mExercise.getStatus().getDisplayName());
            mTxtvStatus.setText(Html.fromHtml(status));

            String progress = String.format(
                    "<font color='#909191'>Progres: </font><font color='#424242'>%s</font>",
                    mExercise.getProgress() + "%");
            mTxtvProgress.setText(Html.fromHtml(progress));

            String time = String.format(
                    "<font color='#909191'>Szacowany czas: </font><font color='#424242'>%s h</font>",
                    mExercise.getAproximateTime());
            mTxtvTime.setText(Html.fromHtml(time));

            String created = String.format(
                    "<font color='#909191'>Utworzono: </font><font color='#424242'>%s</font>",
                    mExercise.getCreated());
            mTxtvCreated.setText(Html.fromHtml(created));
        }

        if (mPerson != null) {
            String person = String.format(
                    "<font color='#909191'>Wykonawca: </font><font color='#424242'>%s</font>",
                    mPerson.getName() + " " + mPerson.getSurname());
            mTxtvPerson.setText(Html.fromHtml(person));
        }

        if (mCampaign != null) {
            String campaign = String.format(
                    "<font color='#909191'>Kampania: </font><font color='#424242'>%s</font>",
                    mCampaign.getName());
            mTxtvCampaign.setText(Html.fromHtml(campaign));
        }
    }

    private void showProgressDialog() {
        ProgressDialogFragment dialogFragment = ProgressDialogFragment.getDialog(mExercise.getProgress(),
                new ProgressListener() {
                    @Override
                    public void onProgressChange(int value) {
                        mExercise.setProgress(value);
                        saveTask();
                        displayData();
                    }
                });
        dialogFragment.show(mActivity.getSupportFragmentManager(), "dialog");
    }

    private void loadPeople() {
        AsyncTask<Void, Void, String[]> loader = new AsyncTask<Void, Void, String[]>() {
            @Override
            protected void onPreExecute() {
                mPersonMap.clear();
            }

            @Override
            protected String[] doInBackground(Void... params) {
                PersonDataSource pds = PersonDataSource.getInstance();
                List<Person> people = pds.getAll();
                String[] array = new String[people.size()];
                int i = 0;

                for (Person person : people) {
                    array[i] = person.getName() + " " + person.getSurname();
                    mPersonMap.put(i, person);
                    i++;
                }

                return array;
            }

            @Override
            protected void onPostExecute(String[] strings) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle("Zmień wykonawcę")
                        .setCancelable(true)
                        .setItems(strings, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mPerson = mPersonMap.get(which);
                                mExercise.setPersonId(mPersonMap.get(which).getId());

                                saveTask();
                                displayData();
                                dialog.dismiss();

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        };

        loader.execute();
    }

    private void saveTask() {
        AsyncTask<Void, Void, Void> saver = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ExerciseDataSource eds = ExerciseDataSource.getInstance();
                eds.insert(mExercise, true);

                return null;
            }
        };

        saver.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        displayData();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
