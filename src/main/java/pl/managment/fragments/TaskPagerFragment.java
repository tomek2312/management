package pl.managment.fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.base.Optional;

import pl.managment.BundleConstants;
import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.ExerciseDataSource;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Exercise;
import pl.managment.db.enitity.Person;
import pl.managment.view.SlidingTabLayout;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class TaskPagerFragment extends RoboEventFragment {
    @InjectView(R.id.view_pager)
    protected ViewPager mViewPager;

    @InjectView(R.id.sliding_tabs)
    private SlidingTabLayout mSlidingTabLayout;

    private MainActivity mActivity;
    private Exercise mExercise;
    private Person mPerson;
    private Campaign mCampaign;
    private TaskPagerDetailsFragment mDetailsFragment;
    private TaskPagerDescriptionFragment mDescriptionFragment;
    private TaskPagerSessionsFragment mSessionsFragment;

    private void setData(Exercise exercise, Person person, Campaign campaign) {
        mExercise = exercise;
        mPerson = person;
        mCampaign = campaign;
    }

    /**
     * Method sets adapter to view pager.
     */
    private void setAdapters() {
        PagerAdapter mPagerAdapter = new PagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        mSlidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        mSlidingTabLayout.setDistributeEvenly(false);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.main_orange);
            }
        });
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.main_blue));
        mPagerAdapter.notifyDataSetChanged();
    }

    /**
     * Method returns tab name according to
     * current view pager position.
     *
     * @param position current position
     * @return current tab name
     */
    private String getTabName(int position) {
        String name = "";
        switch (position) {
            case 0:
                name = "SZCZEGÓŁY";
                break;
            case 1:
                name = "OPIS";
                break;
            case 2:
                name = "AKTYWNOŚCI";
                break;
        }
        return name;
    }

    /**
     * Method creates new instances of fragments.
     */
    private void setFragments() {
        mDetailsFragment = TaskPagerDetailsFragment.getInstance(mExercise, mPerson, mCampaign);
        mDescriptionFragment = TaskPagerDescriptionFragment.getInstance(mExercise);
        mSessionsFragment = TaskPagerSessionsFragment.getInstance(mExercise);
    }

    public static TaskPagerFragment getInstance(Exercise exercise, Person person, Campaign campaign) {
        TaskPagerFragment fragment = new TaskPagerFragment();
        fragment.setData(exercise, person, campaign);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sliding_tab, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFragments();
        setAdapters();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem register = menu.findItem(R.id.action_session);
        Optional<String> profile = MyApplication.loadFromSettings(BundleConstants.LOGIN_PERSON_ID);
        if (profile.isPresent() && mPerson != null) {
            int id = Integer.parseInt(profile.get());
            if (id == mPerson.getId()) {
                register.setVisible(true);
            } else {
                register.setVisible(false);
            }
        } else {
            register.setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);
    }

    private void delete() {
        AsyncTask<Void, Void, Void> deleter = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ExerciseDataSource cds = ExerciseDataSource.getInstance();
                cds.delete(mExercise.getId());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mActivity.onBackPressed();
            }
        };

        deleter.execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mActivity.getMenuInflater().inflate(R.menu.menu_task, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_delete) {
            delete();

            return true;
        }

        if (id == R.id.action_session) {
            mActivity.setFragment(NewSessionFragment.getInstance(mExercise.getId()), true);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {
        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment item;
            switch (position) {
                case 0:
                    item = mDetailsFragment;
                    break;
                case 1:
                    item = mDescriptionFragment;
                    break;
                case 2:
                    item = mSessionsFragment;
                    break;
                default:
                    item = new Fragment();
                    break;
            }

            return item;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getTabName(position);
        }
    }
}
