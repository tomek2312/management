package pl.managment.fragments;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import pl.managment.DateUtils;
import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.ExerciseDataSource;
import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Exercise;
import pl.managment.db.enitity.Person;
import pl.managment.db.enums.ExerciseStatus;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class NewTaskFragment extends RoboEventFragment {
    private static final int MAX_HOURS = 16;
    @InjectView(R.id.edtTaskName)
    private EditText mEdtName;

    @InjectView(R.id.edtTaskDescription)
    private EditText mEdtDescription;

    @InjectView(R.id.sTaskPriority)
    private Spinner mSPriority;

    @InjectView(R.id.sTaskPerson)
    private Spinner mSPerson;

    @InjectView(R.id.npTask)
    private NumberPicker mNumberPicker;

    @InjectView(R.id.btnPlus)
    private TextView mBtnPlus;

    @InjectView(R.id.btnMinus)
    private TextView mBtnMinus;

    private Campaign mCampaign;
    private MainActivity mActivity;
    private HashMap<Integer, Integer> mIdMap = new HashMap<>();

    private void setCampaign(Campaign campaign) {
        mCampaign = campaign;
    }

    private void setPicker() {
        mNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        enableNumberPickerManualEditing(mNumberPicker);
        mNumberPicker.setMinValue(0);
        mNumberPicker.setMaxValue(MAX_HOURS - 1);
        mNumberPicker.setDisplayedValues(getStringItems());
        mNumberPicker.setValue(8);
        mNumberPicker.setEnabled(false);
        mNumberPicker.setWrapSelectorWheel(true);
        setDividerColor(mNumberPicker, MyApplication.getAppContext().getResources().getColor(
                android.R.color.white));

        mBtnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNumberPicker.setValue(mNumberPicker.getValue() + 1);
            }
        });

        mBtnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNumberPicker.setValue(mNumberPicker.getValue() - 1);
            }
        });

    }

    private String[] getStringItems() {
        String[] array = new String[MAX_HOURS];
        for (int i = 0; i < MAX_HOURS; i++) {
            array[i] = Integer.toString(i + 1);
        }

        return array;
    }

    private void enableNumberPickerManualEditing(NumberPicker numPicker) {
        View.OnFocusChangeListener fcl = new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                // Do nothing to suppress keyboard
            }
        };

        int childCount = numPicker.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childView = numPicker.getChildAt(i);

            if (childView instanceof EditText) {
                EditText editText = (EditText) childView;
                editText.setFocusable(false);
                editText.setEnabled(false);
                editText.setOnFocusChangeListener(fcl);
                editText.setInputType(InputType.TYPE_NULL);

                return;
            }
        }
    }

    private void setDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields)
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException | Resources.NotFoundException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
    }

    private void setAdapter() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mActivity,
                R.array.exercise_priority, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSPriority.setAdapter(adapter);
        mSPriority.setSelection(1);
    }

    private void saveTask() {
        AsyncTask<Void, Void, Void> saver = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Exercise exercise = new Exercise();
                exercise.setCampaignId(mCampaign.getId());
                exercise.setDescription(mEdtDescription.getText().toString());
                exercise.setName(mEdtName.getText().toString());
                exercise.setStatus(ExerciseStatus.CREATE.getName());
                exercise.setCreated(DateUtils.getCurrentDate());
                exercise.setProgress(0);
                exercise.setPriority((String) mSPriority.getSelectedItem());
                exercise.setPersonId(mIdMap.get(mSPerson.getSelectedItemPosition()));
                exercise.setAproximateTime(Integer.toString(mNumberPicker.getValue() + 1));

                ExerciseDataSource eds = ExerciseDataSource.getInstance();
                eds.insert(exercise, false);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mActivity.onBackPressed();
            }
        };

        saver.execute();
    }

    private void loadPeople() {
        AsyncTask<Void, Void, String[]> loader = new AsyncTask<Void, Void, String[]>() {
            @Override
            protected void onPreExecute() {
                mIdMap.clear();
            }

            @Override
            protected String[] doInBackground(Void... params) {
                PersonDataSource pds = PersonDataSource.getInstance();
                List<Person> people = pds.getAll();
                String[] array = new String[people.size() + 1];
                array[0] = "---- ----";
                mIdMap.put(0, -1);
                int i = 1;

                for (Person person : people) {
                    array[i] = person.getName() + " " + person.getSurname();
                    mIdMap.put(i, person.getId());
                    i++;
                }

                return array;
            }

            @Override
            protected void onPostExecute(String[] strings) {
                ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(mActivity,
                        android.R.layout.simple_spinner_item, strings);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mSPerson.setAdapter(adapter);
            }
        };

        loader.execute();
    }

    public static NewTaskFragment createInstance(Campaign campaign) {
        NewTaskFragment fragment = new NewTaskFragment();
        fragment.setCampaign(campaign);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_task, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadPeople();
        setAdapter();
        setPicker();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mActivity.getMenuInflater().inflate(R.menu.menu_save_task, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            saveTask();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
