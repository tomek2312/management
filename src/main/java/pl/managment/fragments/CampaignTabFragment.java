package pl.managment.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pl.managment.MainActivity;
import pl.managment.R;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Client;
import pl.managment.db.enitity.Person;
import pl.managment.db.enitity.Tag;
import pl.managment.view.SlidingTabLayout;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class CampaignTabFragment extends RoboEventFragment {
    @InjectView(R.id.view_pager)
    protected ViewPager mViewPager;

    @InjectView(R.id.sliding_tabs)
    private SlidingTabLayout mSlidingTabLayout;

    private CampaignInfoFragment mInfoFragment;
    private CampaignTasksFragment mTasksFragment;
    private MainActivity mActivity;
    private Client mClient;
    private Person mManager;
    private Campaign mCampaign;
    private List<Tag> mTags;

    private void setData(Client client, Campaign campaign, List<Tag> tags, Person manager) {
        mCampaign = campaign;
        mClient = client;
        mTags = tags;
        mManager = manager;
    }

    /**
     * Method sets adapter to view pager.
     */
    private void setAdapters() {
        PagerAdapter mPagerAdapter = new PagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        mSlidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.main_orange);
            }
        });
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.main_blue));
        mPagerAdapter.notifyDataSetChanged();
    }

    /**
     * Method returns tab name according to
     * current view pager position.
     *
     * @param position current position
     * @return current tab name
     */
    private String getTabName(int position) {
        String name = "";
        switch (position) {
            case 0:
                name = "INFORMACJE";
                break;
            case 1:
                name = "ZADANIA";
                break;
        }
        return name;
    }

    /**
     * Method creates new instances of fragments.
     */
    private void setFragments() {
        mInfoFragment = CampaignInfoFragment.getInstance(mCampaign, mClient, mTags, mManager);
        mTasksFragment = CampaignTasksFragment.getInstance(mCampaign);
    }

    public static CampaignTabFragment getInstance(Campaign campaign, Client client, List<Tag> tags,
                                                  Person manager) {
        CampaignTabFragment fragment = new CampaignTabFragment();
        fragment.setData(client, campaign, tags, manager);

        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFragments();
        setAdapters();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sliding_tab, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {
        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment item;
            switch (position) {
                case 0:
                    item = mInfoFragment;
                    break;
                case 1:
                    item = mTasksFragment;
                    break;
                default:
                    item = new Fragment();
                    break;
            }

            return item;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getTabName(position);
        }
    }
}
