package pl.managment.fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Optional;
import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import pl.managment.ImageUtils;
import pl.managment.MainActivity;
import pl.managment.R;
import pl.managment.db.CampaignDataSource;
import pl.managment.db.enitity.Campaign;
import pl.managment.db.enitity.Client;
import pl.managment.db.enitity.Person;
import pl.managment.db.enitity.Tag;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class CampaignInfoFragment extends RoboEventFragment {
    @InjectView(R.id.imgvCampaignAvatar)
    private ImageView mImgvAvatar;

    @InjectView(R.id.imgvCampaignAvatarEdit)
    private ImageView mImgvAvatarEdit;

    @InjectView(R.id.txtvCampaignTitle)
    private TextView mTxtvTitle;

    @InjectView(R.id.txtvCampaignStart)
    private TextView mTxtvStart;

    @InjectView(R.id.txtvCampaignEnd)
    private TextView mTxtvEnd;

    @InjectView(R.id.txtvCampaignManager)
    private TextView mTxtvManager;

    @InjectView(R.id.txtvCampaignDescription)
    private TextView mTxtvDescription;

    @InjectView(R.id.txtvCampaignTags)
    private TextView mTxtvTags;

    @InjectView(R.id.txtvCampaignClientName)
    private TextView mTxtvClientName;

    @InjectView(R.id.txtvCampaignClientPhone)
    private TextView mTxtvClientPhone;

    @InjectView(R.id.txtvCampaignClientPerson)
    private TextView mTxtvClientPerson;

    @Inject
    private ImageUtils mImageUtils;

    private MainActivity mActivity;
    private Client mClient;
    private Person mManager;
    private Campaign mCampaign;
    private List<Tag> mTags;

    private void setData(Client client, Campaign campaign, List<Tag> tags, Person manager) {
        mCampaign = campaign;
        mClient = client;
        mTags = tags;
        mManager = manager;
    }

    private void loadContent() {
        String start = String.format(
                "<font color='#909191'>Data rozpoczęcia: </font><font color='#424242'>%s</font>",
                mCampaign.getStartDate());
        mTxtvStart.setText(Html.fromHtml(start));

        String end = String.format(
                "<font color='#909191'>Data zakończenia: </font><font color='#424242'>%s</font>",
                mCampaign.getEndDate());
        mTxtvEnd.setText(Html.fromHtml(end));

        if (mManager != null) {
            String manager = String.format(
                    "<font color='#909191'>Manager: </font><font color='#424242'>%s</font>",
                    mManager.getName() + " " + mManager.getSurname());
            mTxtvManager.setText(Html.fromHtml(manager));
        }

        if (mClient != null) {
            String clientName = String.format(
                    "<font color='#909191'>Nazwa: </font><font color='#424242'>%s</font>",
                    mClient.getName());
            mTxtvClientName.setText(Html.fromHtml(clientName));

            String clientPhone = String.format(
                    "<font color='#909191'>Telefon: </font><font color='#424242'>%s</font>",
                    mClient.getPhone());
            mTxtvClientPhone.setText(Html.fromHtml(clientPhone));

            String clientPerson = String.format(
                    "<font color='#909191'>Osoba kontaktowa: </font><font color='#424242'>%s</font>",
                    mClient.getPerson());
            mTxtvClientPerson.setText(Html.fromHtml(clientPerson));
        }

        mTxtvTitle.setText(mCampaign.getName());
        mTxtvDescription.setText(mCampaign.getDescription());

        String tags = "";
        for (int i = 0; i < mTags.size(); i++) {
            tags += mTags.get(i).getName();
            if (i != mTags.size() - 1) {
                tags += ", ";
            }
        }

        mTxtvTags.setText(tags);
    }

    private void delete() {
        AsyncTask<Void, Void, Void> deleter = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                CampaignDataSource cds = CampaignDataSource.getInstance();
                cds.delete(mCampaign.getId());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mActivity.onBackPressed();
            }
        };

        deleter.execute();
    }

    private void setDrawer() {
        AsyncTask<Void, Void, Void> loader = new AsyncTask<Void, Void, Void>() {
            private Bitmap mAvatar;

            @Override
            protected void onPreExecute() {
                mImgvAvatar.setVisibility(View.INVISIBLE);
                mAvatar = BitmapFactory.decodeResource(getResources(), R.drawable.campaign_placeholder);
            }

            @Override
            protected Void doInBackground(Void... params) {
                String avatar = mCampaign.getAvatar();
                if (avatar != null && !TextUtils.isEmpty(avatar)) {
                    int size = (int) getResources().getDimension(R.dimen.dashboard_image_big);

                    File file = new File(avatar);
                    try {
                        Optional<Bitmap> optBitmap = mImageUtils.loadPreScaledBitmap(file,
                                size, size);
                        if (optBitmap.isPresent()) {
                            mAvatar = mImageUtils.transformToCircle(optBitmap.get());
                        } else {
                            mAvatar = mImageUtils.transformToCircle(mAvatar);
                        }
                    } catch (IOException e) {
                        mAvatar = mImageUtils.transformToCircle(mAvatar);
                    }
                } else {
                    mAvatar = mImageUtils.transformToCircle(mAvatar);
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mImgvAvatar.setVisibility(View.VISIBLE);
                mImgvAvatar.setImageBitmap(mAvatar);
            }
        };

        loader.execute();
    }

    public static CampaignInfoFragment getInstance(Campaign campaign, Client client, List<Tag> tags,
                                                   Person manager) {
        CampaignInfoFragment fragment = new CampaignInfoFragment();
        fragment.setData(client, campaign, tags, manager);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_campaign_info, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setDrawer();
        loadContent();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mCampaign != null) {
            mActivity.getMenuInflater().inflate(R.menu.menu_delete, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_delete) {
            delete();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
