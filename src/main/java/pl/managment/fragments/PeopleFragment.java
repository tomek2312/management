package pl.managment.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pl.managment.ImageUtils;
import pl.managment.MainActivity;
import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.PersonDataSource;
import pl.managment.db.enitity.Person;
import pl.managment.db.enums.PersonFunction;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class PeopleFragment extends RoboEventFragment {
    /*package*/ static class PersonListAdapter extends ArrayAdapter<Person> {
        private final Context mContext;

        private final ArrayList<Person> mData;
        private final int mLayout;

        private class ViewHolder {
            public ImageView mPersonAvatar;
            public TextView mName;
            public TextView mFunction;
        }

        public PersonListAdapter(Context context, ArrayList<Person> data, int layout) {
            super(context, layout, data);

            mContext = context;
            mData = data;
            mLayout = layout;
        }

        private void loadPerson(ImageView imageView, TextView textView,
                                TextView textFunction, Person person) {
            if (person.getFunction() == PersonFunction.ADMIN) {
                textFunction.setText("Administrator");
            } else if (person.getFunction() == PersonFunction.MANAGER) {
                textFunction.setText("Manager");
            } else {
                textFunction.setText("Pracownik");
            }

            textView.setText(person.getName() + " " + person.getSurname());

            try {
                loadAvatar(person.getAvatar(), imageView, person.getId());
            } catch (IOException e) {
                imageView.setImageResource(R.drawable.male_avatar);
            }

        }

        private void loadAvatar(final String avatar, final ImageView imageView, final int personId)
                throws IOException {
            AsyncTask<Void, Void, Void> loader = new AsyncTask<Void, Void, Void>() {
                private Bitmap mBitmap = BitmapFactory.decodeResource(
                        MyApplication.getAppContext().getResources(),
                        R.drawable.male_avatar);

                @Override
                protected void onPreExecute() {
                    imageView.setVisibility(View.INVISIBLE);
                }

                @Override
                protected Void doInBackground(Void... params) {
                    if (avatar != null && !TextUtils.isEmpty(avatar)) {
                        File tempImage = new File(avatar);
                        if (tempImage.exists()) {
                            int size = (int) MyApplication.getAppContext().getResources().
                                    getDimension(R.dimen.dashboard_image_small);
                            ImageUtils utils = new ImageUtils();
                            mBitmap = utils.getResizedBitmap(size, size, tempImage.getAbsolutePath());
                            mBitmap = utils.getRoundedTaskBitmap(mBitmap);
                        }
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    if (imageView.getTag() instanceof Integer && personId == (Integer) imageView.getTag()) {
                        imageView.setImageBitmap(mBitmap);
                        imageView.setVisibility(View.VISIBLE);
                    }
                }
            };

            loader.execute();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(mLayout, viewGroup, false);
                viewHolder = new ViewHolder();

                viewHolder.mPersonAvatar = (ImageView) convertView.findViewById(R.id.imgvPersonImage);
                viewHolder.mName = (TextView) convertView.findViewById(R.id.txtvPersonName);
                viewHolder.mFunction = (TextView) convertView.findViewById(R.id.txtvPersonFunction);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Person person = mData.get(position);
            viewHolder.mPersonAvatar.setTag(person.getId());
            loadPerson(viewHolder.mPersonAvatar, viewHolder.mName, viewHolder.mFunction, person);

            return convertView;
        }
    }

    @InjectView(R.id.lvProjects)
    private ListView mListView;

    @InjectView(R.id.pbProjects)
    private ProgressBar mProgressBar;

    @Inject
    private ImageUtils mImageUtils;

    private PersonListAdapter mListAdapter;
    protected ArrayList<Person> mPeople = new ArrayList<>();
    private MainActivity mActivity;
    private int mPersonId;

    private void setId(int personId) {
        mPersonId = personId;
    }

    private void setAdapter() {
        mListAdapter = new PersonListAdapter(MyApplication.getAppContext(),
                mPeople, R.layout.list_row_person);
        mListView.setAdapter(mListAdapter);
    }

    private void setListeners() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mActivity.setFragment(ProfileFragment.getInstance(mPeople.get(position).getId()), true);
            }
        });
    }

    private void loadTasks() {
        AsyncTask<Void, Void, Void> loaderTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                mListView.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                mPeople.clear();
            }

            @Override
            protected Void doInBackground(Void... params) {
                PersonDataSource pds = PersonDataSource.getInstance();
                List<Person> people = pds.getAll();

                for (Person person : people) {
                    if (mPersonId != person.getId()) {
                        mPeople.add(person);
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mListAdapter.notifyDataSetChanged();
                mProgressBar.setVisibility(View.GONE);
                mListView.setVisibility(View.VISIBLE);
            }
        };
        loaderTask.execute();
    }

    private void setToolbarTitle() {
        mActivity.setActionBarTitle("Pracownicy");
    }

    public static PeopleFragment getInstance(int personId) {
        PeopleFragment fragment = new PeopleFragment();
        fragment.setId(personId);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_people, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
        loadTasks();
        setListeners();
        setToolbarTitle();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mActivity.getMenuInflater().inflate(R.menu.menu_new_task, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add) {
            mActivity.setFragment(new AddPersonFragment(), true);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
