package pl.managment.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.ImageView;

import pl.managment.R;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Custom view, that animates and shows current
 * value of user progress.
 *
 * @author Tomasz Trybała
 */
public class FillImageView extends ImageView {
    //region variables
    private int mHeight;
    private int mWidth;
    private int mRight;
    private Paint mPaint;
    private Paint mTextPaint;
    private Paint mBgPaint;

    private boolean mWasSet;
    private int mProgress = -1;
    //endregion

    //region default constructors
    public FillImageView(Context context) {
        super(context);
        setPaint();
    }

    public FillImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setPaint();
    }

    public FillImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setPaint();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mHeight = h;
        mWidth = w;
        if (!mWasSet && mProgress != -1) {
            animateView(mProgress);
        }
    }

    private void drawText(Canvas canvas, String text) {
        int xPos = (int)(canvas.getWidth() - mTextPaint.getTextSize() * Math.abs(text.length() / 2)) / 2;
        int yPos = (int) ((canvas.getHeight() / 2) - ((mTextPaint.descent() + mTextPaint.ascent()) / 2));
        canvas.drawText(text, xPos, yPos, mTextPaint);
    }

    private void setPaint() {
        mPaint = new Paint(Paint.DITHER_FLAG);
        mBgPaint = new Paint(Paint.DITHER_FLAG);
        mBgPaint.setColor(Color.rgb(200, 200, 200));

        mTextPaint = new Paint();
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setTextSize(getResources().getDimensionPixelSize(R.dimen.font_small));
    }

    /**
     * Method animates current view.
     *
     * @param progress target progress
     */
    public void animateView(int progress) {
        checkArgument(progress >= 0, "progress must be not less than 0");
        checkArgument(progress <= 100, "progress must be not greater than 100");
        mProgress = progress;
        if (mWidth != 0) {
            mRight = (int) (((float) progress / 100.0f) * (float) mWidth);
            int g = (int) (((float) progress * 255f) / 100f);
            int r = (int) ((255f - (progress * 255f) / 100f));
            mPaint.setColor(Color.rgb(r, g, 0));
            mWasSet = true;
            invalidate();
        }
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        canvas.drawRect(0, 0, mWidth, mHeight, mBgPaint);
        canvas.drawRect(0, 0, mRight, mHeight, mPaint);
        drawText(canvas, mProgress + "%");
    }
}
