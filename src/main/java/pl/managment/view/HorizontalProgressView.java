package pl.managment.view;

import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.NinePatchDrawable;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;


import pl.managment.R;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * @author Tomasz Trybała
 */
public class HorizontalProgressView extends View {
    private static final int ANIMATION_DURATION = 1500;

    private int mProgress;
    private Paint mPaint;
    private Paint mTextPaint;
    private NinePatchDrawable mNinePatch;
    private Bitmap mWellbitBitmap;
    private Path mPath;
    private int mBitmapWidth;
    private float mPaddingDefault = getResources().getDimension(R.dimen.padding_default) * 1.5f;
    private boolean mDrawCoin = true;
    private boolean mPercentageVisible = true;

    private void setParams(@NonNull Context context) {
        mNinePatch = (NinePatchDrawable)
                getResources().getDrawable(R.drawable.bg_horizontal_progress);

        mWellbitBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.progress_bar_wellbit);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        float width = (float) mWellbitBitmap.getHeight();
        mPaint.setStrokeWidth(width);
        mPaint.setColor(context.getResources().getColor(android.R.color.holo_red_dark));
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);

        mTextPaint = new Paint();
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setTextSize(mPaint.getStrokeWidth() * 0.8f);

        mPath = new Path();
    }

    public void setProgress(int progress) {
        mPath = new Path();
        mProgress = progress;
        float max = mBitmapWidth - mPaddingDefault;
        mPath.moveTo(mPaddingDefault, (mWellbitBitmap.getHeight() / 2));
        float line = max * (mProgress / 100.0f);
        mPath.lineTo(line > mPaddingDefault ? line : mPaddingDefault, (mWellbitBitmap.getHeight() / 2));
        invalidate();
    }

    private void drawText(Canvas canvas) {
        float max = mBitmapWidth - mPaddingDefault;
        float position = (mWellbitBitmap.getHeight() / 3) + (mPaint.getStrokeWidth() / 2);
        float line = max * (mProgress / 100.0f);

        if (mPercentageVisible) {
            if (mProgress > 0) {
                mTextPaint.setColor(Color.BLACK);
                canvas.drawText("" + mProgress + "%",
                        line > mPaddingDefault ? (line / 2) - mPaddingDefault / 2 : mPaddingDefault,
                        position,
                        mTextPaint);
            } else {
                mTextPaint.setColor(Color.GRAY);
                canvas.drawText("" + mProgress + "%",
                        mBitmapWidth / 2 - mPaddingDefault / 2,
                        position,
                        mTextPaint);
            }
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    public HorizontalProgressView(Context context) {
        super(context);
        setParams(context);
    }

    @SuppressWarnings("UnusedDeclaration")
    public HorizontalProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setParams(context);
        parseAndProcessXmlAttrs(context, attrs);
    }

    @SuppressWarnings("UnusedDeclaration")
    public HorizontalProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setParams(context);
        parseAndProcessXmlAttrs(context, attrs);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mBitmapWidth = w;
        mNinePatch.setBounds(0, 0, mBitmapWidth, mWellbitBitmap.getHeight());

        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (heightMeasureSpec == 0) {
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(mWellbitBitmap.getHeight(), MeasureSpec.AT_MOST);
        }

        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
                getDefaultSize(this.getSuggestedMinimumHeight(), heightMeasureSpec));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mNinePatch.draw(canvas);
        canvas.drawPath(mPath, mPaint);
        drawText(canvas);
    }

    public void animateProgress(int progress) {
        checkArgument(progress >= 0, "progress cannot be less than 0");
        checkArgument(progress <= 100, "progress cannot be higher than 100");

        ValueAnimator animator = ValueAnimator.ofInt(0, progress);
        animator.setDuration(ANIMATION_DURATION);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                //noinspection ConstantConditions
                setProgress((Integer) animation.getAnimatedValue());
            }
        });
        animator.start();
    }

    public void animateProgress(int progress, AnimatorListenerAdapter listener) {
        checkArgument(progress >= 0, "progress cannot be less than 0");
        checkArgument(progress <= 100, "progress cannot be higher than 100");

        ValueAnimator animator = ValueAnimator.ofInt(0, progress);
        animator.setDuration(ANIMATION_DURATION);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                //noinspection ConstantConditions
                setProgress((Integer) animation.getAnimatedValue());
            }
        });
        animator.addListener(listener);
        animator.start();

    }

    public int getProgress() {
        return mProgress;
    }

    private void parseAndProcessXmlAttrs(Context context, AttributeSet attrs) {
        if (context != null && context.getTheme() != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.HorizontalProgressView,
                    0,
                    0
            );

            try {
                if (a != null) {
                    int progress = a.getInt(R.styleable.HorizontalProgressView_horizontal_progress, 0);
                    boolean coin = a.getBoolean(R.styleable.HorizontalProgressView_coin, true);
                    boolean percentageVisible = a.getBoolean(R.styleable.HorizontalProgressView_percentage_visible, true);

                    checkArgument(progress >= 0, "progress cannot be less than 0");
                    checkArgument(progress <= 100, "progress cannot be less than 0");
                    mProgress = progress;
                    mDrawCoin = coin;
                    mPercentageVisible = percentageVisible;
                }
            } finally {
                if (a != null) {
                    a.recycle();
                }
            }
        }
    }
}
