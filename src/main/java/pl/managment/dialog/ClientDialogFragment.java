package pl.managment.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import pl.managment.R;
import pl.managment.db.ClientDataSource;
import pl.managment.db.enitity.Client;
import pl.managment.listeners.ClientListener;
import roboguice.fragment.RoboDialogFragment;

/**
 * @author Tomasz Trybała
 */
public class ClientDialogFragment extends RoboDialogFragment {
    private AlertDialog mDialog;
    private EditText mEdtName;
    private EditText mEdtPerson;
    private EditText mEdtPhone;
    private Button mButton;
    private ClientListener mListener;
    private Client mClient;

    private void setData(ClientListener listener) {
        mListener = listener;
    }

    public static ClientDialogFragment getDialog(ClientListener listener) {
        ClientDialogFragment fragment = new ClientDialogFragment();
        fragment.setData(listener);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        System.out.println("On Create Dialog");
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_client, null, false);

        mEdtName = (EditText) dialogView.findViewById(R.id.edtClientName);
        mEdtPerson = (EditText) dialogView.findViewById(R.id.edtClientPerson);
        mEdtPhone = (EditText) dialogView.findViewById(R.id.edtClientPhone);
        mButton = (Button) dialogView.findViewById(R.id.btnProgress);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        mDialog = dialogBuilder.create();
        mDialog.setCancelable(true);
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        setListeners();

        return mDialog;
    }

    private void saveClient() {
        mClient = new Client();
        mClient.setName(mEdtName.getText().toString());
        mClient.setPerson(mEdtPerson.getText().toString());
        mClient.setPhone(mEdtPhone.getText().toString());

        AsyncTask<Void, Void, Void> saver = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ClientDataSource cds = ClientDataSource.getInstance();
                cds.insert(mClient, false);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mDialog.dismiss();
                mListener.onCreated(mClient);
            }
        };

        saver.execute();

    }

    private void setListeners() {
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveClient();
            }
        });
    }
}
