package pl.managment.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import pl.managment.MyApplication;
import pl.managment.R;
import pl.managment.db.enitity.Person;
import pl.managment.db.enums.PersonFunction;
import pl.managment.listeners.ProfileListener;
import roboguice.fragment.RoboDialogFragment;

/**
 * @author Tomasz Trybała
 */
public class ProfileChangeDialog extends RoboDialogFragment {
    private AlertDialog mDialog;
    private ProfileListener mListener;
    private Person mPerson;
    private String mFunction;
    private boolean mIsAdmin;

    private EditText mEdtPhone;
    private EditText mEdtAddress;
    private EditText mEdtPassword;
    private DatePicker mDatePicker;
    private Spinner mSFunction;
    private Button mBtnAccept;

    private void setData(Person person, String function, boolean isAdmin, ProfileListener listener) {
        mPerson = person;
        mIsAdmin = isAdmin;
        mFunction = function;
        mListener = listener;
    }

    public static ProfileChangeDialog getDialog(Person person, String function, boolean isAdmin, ProfileListener listener) {
        ProfileChangeDialog fragment = new ProfileChangeDialog();
        fragment.setData(person, function, isAdmin, listener);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_profile_change, null, false);

        mEdtPhone = (EditText) dialogView.findViewById(R.id.edtProfilePhone);
        mEdtAddress = (EditText) dialogView.findViewById(R.id.edtProfileAddress);
        mEdtPassword = (EditText) dialogView.findViewById(R.id.edtProfilePassword);
        mDatePicker = (DatePicker) dialogView.findViewById(R.id.datePicker);
        mSFunction = (Spinner) dialogView.findViewById(R.id.sProfileFunction);
        mBtnAccept = (Button) dialogView.findViewById(R.id.btnAccept);


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        mDialog = dialogBuilder.create();
        mDialog.setCancelable(true);
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        setAdapters();
        setData();


        return mDialog;
    }

    private String getFunction() {
        int item = mSFunction.getSelectedItemPosition();
        if (item == 0) {
            return PersonFunction.ADMIN.getName();
        } else if (item == 1) {
            return PersonFunction.MANAGER.getName();
        } else {
            return PersonFunction.WORKER.getName();
        }
    }

    private void setAdapters() {
        if (mIsAdmin) {
            ArrayAdapter<CharSequence> adapterFunction = ArrayAdapter.createFromResource(
                    MyApplication.getAppContext(),
                    R.array.array_function, android.R.layout.simple_spinner_item);
            adapterFunction.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSFunction.setAdapter(adapterFunction);

            if (mFunction.equals(PersonFunction.ADMIN.getName())) {
                mSFunction.setSelection(0);
            } else if (mFunction.equals(PersonFunction.MANAGER.getName())) {
                mSFunction.setSelection(1);
            } else {
                mSFunction.setSelection(2);
            }
        } else {
            mSFunction.setVisibility(View.GONE);
        }
    }

    private static void setTextColorBlack(ViewGroup v) {
        int count = v.getChildCount();
        for (int i = 0; i < count; i++) {
            View c = v.getChildAt(i);
            if(c instanceof ViewGroup){
                setTextColorBlack((ViewGroup) c);
            } else
            if(c instanceof EditText){
                ((EditText) c).setTextColor(Color.BLACK);
            }
        }
    }

    private void setData() {
        mEdtAddress.setText(mPerson.getAddress());
        mEdtPhone.setText(mPerson.getPhone());
        mEdtPassword.setText(mPerson.getPassword());
        String[] date = mPerson.getDateBirth().split("-");
        mDatePicker.init(Integer.parseInt(date[2]), Integer.parseInt(date[1]), Integer.parseInt(date[0]), null);

        mBtnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onChange(mIsAdmin ? getFunction() : null,
                        (mDatePicker.getDayOfMonth() + "-" +
                                mDatePicker.getMonth() + "-" +
                                mDatePicker.getYear()), mEdtAddress.getText().toString(),
                        mEdtPhone.getText().toString(), mEdtPassword.getText().toString());

                InputMethodManager inputManager = (InputMethodManager) MyApplication.getAppContext().getSystemService(
                        Context.INPUT_METHOD_SERVICE);

                View view = mDialog.getCurrentFocus();
                if (view != null) {
                    inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }

                mDialog.dismiss();
            }
        });
    }
}
