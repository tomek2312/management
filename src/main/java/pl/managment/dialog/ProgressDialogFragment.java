package pl.managment.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import pl.managment.R;
import pl.managment.listeners.ProgressListener;
import roboguice.fragment.RoboDialogFragment;

/**
 * @author Tomasz Trybała
 */
public class ProgressDialogFragment extends RoboDialogFragment {
    private AlertDialog mDialog;
    private ImageView mImgvPlus;
    private ImageView mImgvMinus;
    private TextView mTxtvProgress;
    private SeekBar mSeekBar;
    private Button mButton;
    private int mCurrentProgress;
    private ProgressListener mListener;

    private void setData(int currentProgress, ProgressListener listener) {
        mCurrentProgress = currentProgress;
        mListener = listener;
    }

    public static ProgressDialogFragment getDialog(int currentProgress, ProgressListener listener) {
        ProgressDialogFragment fragment = new ProgressDialogFragment();
        fragment.setData(currentProgress, listener);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.dialog_progress, null, false);

        mImgvMinus = (ImageView) dialogView.findViewById(R.id.imgvProgressMinus);
        mImgvPlus = (ImageView) dialogView.findViewById(R.id.imgvProgressPlus);
        mTxtvProgress = (TextView) dialogView.findViewById(R.id.txtvProgress);
        mButton = (Button) dialogView.findViewById(R.id.btnProgress);
        mSeekBar = (SeekBar) dialogView.findViewById(R.id.sbProgress);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        mDialog = dialogBuilder.create();
        mDialog.setCancelable(true);
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        setListeners();

        return mDialog;
    }

    private void setListeners() {
        mImgvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSeekBar.setProgress(mSeekBar.getProgress() + 1);
            }
        });

        mImgvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSeekBar.setProgress(mSeekBar.getProgress() - 1);
            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onProgressChange(mSeekBar.getProgress());
                mDialog.dismiss();
            }
        });

        mSeekBar.setMax(100);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mCurrentProgress = progress;
                setProgressOnViews();
            }
        });

        mSeekBar.setProgress(mCurrentProgress);
        setProgressOnViews();
    }

    private void setProgressOnViews() {
        mTxtvProgress.setText(mCurrentProgress + "%");
    }
}
