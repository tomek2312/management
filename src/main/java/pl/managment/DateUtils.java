package pl.managment;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;

/**
 * Class allows user to coordinate
 * all functions in application that depend
 * on time.
 *
 * @author Tomasz Trybała
 */
public class DateUtils {
    public static final String FORMAT_HOUR_MINUTE = "HH:mm";
    public static final String FORMAT_HOUR_MINUTE_SECOND = "HH:mm:ss";
    public static final String FORMAT_YEAR_MONTH_DAY = "yyyy-MM-dd";
    public static final String FORMAT_HEADER = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_SESSION = "dd-MM-yyyy HH:mm";
    public static final String FORMAT_SESSION_DATE = "dd-MM-yyyy";

    /**
     * Method checks is actual day is saturday or sunday.
     *
     * @return true when day is saturday or sunday
     * false when is different day
     */
    public static boolean isWeekend() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        return day == Calendar.SATURDAY || day == Calendar.SUNDAY;
    }


    /**
     * Method returns formatted current time.
     *
     * @return formatted current time
     */
    public static String getCurrentTime() {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(FORMAT_HOUR_MINUTE);
        return formatter.print(DateTime.now());
    }

    /**
     * Method returns formatted current time.
     *
     * @return formatted current time
     */
    public static String getCurrentSessionDate() {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(FORMAT_SESSION_DATE);
        return formatter.print(DateTime.now());
    }

    /**
     * Method returns formatted current time to header.
     *
     * @return formatted current time
     */
    public static String getHeaderTime() {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(FORMAT_HEADER);
        return formatter.print(DateTime.now());
    }

    /**
     * Method returns formatted current date.
     *
     * @return formatted current date
     */
    public static String getCurrentDate() {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(FORMAT_SESSION);
        return formatter.print(DateTime.now());
    }

    public static String getSessionTime(String start, String stop) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(FORMAT_SESSION);
        DateTime startTime = formatter.parseDateTime(start);
        DateTime stopTime = formatter.parseDateTime(stop);
        int minutes = Minutes.minutesBetween(startTime, stopTime).getMinutes();

        if (minutes < 60) {
            return String.format("%d min", minutes);
        } else {
            int hours = minutes / 60;
            if (minutes % 60 == 0) {
                return String.format("%d h", hours);
            } else {
                return String.format("%d h %d min", hours, minutes % 60);
            }
        }
    }
}
