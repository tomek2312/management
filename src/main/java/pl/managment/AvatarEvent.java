package pl.managment;

/**
 *@author Tomasz Trybała
 */
public class AvatarEvent {
    private String mPath;

    public AvatarEvent(String path) {
        this.mPath = path;
    }

    public String getPath() {
        return mPath;
    }
}
