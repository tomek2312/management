package pl.managment;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.common.base.Optional;
import com.google.common.eventbus.EventBus;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *@author Tomasz Trybała
 */
public class MyApplication extends Application{
    public static final String SETTINGS_NAME = "appSettings";
    private static EventBus sEventBus;
    private static Context sContext;
    private RequestQueue mRequestQueue;
    private static MyApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        sEventBus = new EventBus("management event bus");
        sContext = getApplicationContext();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public static synchronized MyApplication getInstance() {
        return sInstance;
    }

    public static @NonNull EventBus getEventBus() {
        return sEventBus;
    }

    public static @NonNull Context getAppContext() {
        return sContext;
    }

    public static void saveToSettings(@NonNull String key, @NonNull String value) {
        checkNotNull(key, "key cannot be null");
        checkNotNull(value, "value cannot be null");

        SharedPreferences settings = sContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        StringEncryptor encryptor = new StringEncryptor();
        Optional<String> encryptedValue = encryptor.encrypt(value);

        if (encryptedValue.isPresent()) {
            String encrypted = encryptedValue.get();
            preferencesEditor.putString(key, encrypted);
        } else {
            preferencesEditor.putString(key, value);
        }

        preferencesEditor.apply();
    }

    public static @NonNull Optional<String> loadFromSettings(@NonNull String key) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = sContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        String encryptedPassword = settings.getString(key, null);
        String decryptedValue = encryptedPassword;

        if (encryptedPassword != null) {
            StringEncryptor encryptor = new StringEncryptor();
            Optional<String> decryptedPassword = encryptor.decrypt(encryptedPassword);
            if (decryptedPassword.isPresent()) {
                decryptedValue = decryptedPassword.get();
            }
        }

        return Optional.fromNullable(decryptedValue);
    }

    public static void clearSetting(@NonNull String key) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = sContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.apply();
    }
}
