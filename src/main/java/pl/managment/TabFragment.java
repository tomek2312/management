package pl.managment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import pl.managment.fragments.CampaignTaskPagerFragment;
import pl.managment.fragments.PeopleFragment;
import pl.managment.fragments.ProfileFragment;
import pl.managment.fragments.ProjectsListFragment;
import pl.managment.fragments.RoboEventFragment;
import roboguice.inject.InjectView;

/**
 * @author Tomasz Trybała
 */
public class TabFragment extends RoboEventFragment {
    @InjectView(R.id.llMainProfile)
    private LinearLayout mLlProfile;

    @InjectView(R.id.llMainProjects)
    private LinearLayout mLlProjects;

    @InjectView(R.id.llMainExercises)
    private LinearLayout mLlExercises;

    @InjectView(R.id.llMainPeople)
    private LinearLayout mLlPeople;

    @InjectView(R.id.llMainLogout)
    private LinearLayout mLlLogout;

    private int mPersonId;
    private MainActivity mActivity;

    private void setPersonId(int personId) {
        mPersonId = personId;
    }

    private void setListeners(){
        mLlProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  mActivity.setFragment(ProfileFragment.getInstance(mPersonId), true);
            }
        });

        mLlProjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.setFragment(new ProjectsListFragment(), true);
            }
        });

        mLlExercises.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.setFragment(CampaignTaskPagerFragment.getInstance(mPersonId), true);
            }
        });

        mLlPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.setFragment(PeopleFragment.getInstance(mPersonId), true);
            }
        });

        mLlLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void logout(){
        MyApplication.clearSetting(BundleConstants.LOGIN_PERSON_ID);
        mActivity.finish();
    }

    public static TabFragment getInstance(int personId) {
        TabFragment fragment = new TabFragment();
        fragment.setPersonId(personId);

        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity.setActionBarTitle("Menu");
        setListeners();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tab, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            mActivity = (MainActivity) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }
}
